#! /usr/bin/octave -qf
clc
clear all
close all
more off
addpath ('functions');
config;

terminalOut = Show_Terminal_Outputs;
openFigures = Open_Created_Figures_In_Octave;
createPlots = Create_Graphic_Output_Files;
createTXT = Create_TXT_Summary;

set (0, "defaultaxesfontsize", 16) 
set (0, "defaulttextfontsize", 16)

if(terminalOut)
  printf("--> 1_monitoring_distr.m was started \n \n");
endif

%Testpath
%path2logs = '../../distributed/software/logs/1_monitoring/OctaveTestData';
%path2logs = '../../distributed/software/logs/2_monitoring/0_singleChannel';
%path2logs = '../../distributed/software/logs/2_monitoring/IF1only';

Monitoring_Distributed_Path
printf("\n----------------------------------------------------------------\n");

path2logs = Monitoring_Distributed_Path;

for ChooseFolder=1:1 % for more than one path...
  
%  if( ChooseFolder == 1 )
%    %path2logs = '../../distributed/software/logs/2_monitoring/0_singleChannel';
%  else
%    %path2logs = '../../distributed/software/logs/2_monitoring/1_multiChannel';
%  endif
  
  DistrLogsFolder = dir(path2logs);
  LogFolderList = {DistrLogsFolder.name};

%  for EnterLogFolder=1:(numel(LogFolderList))
%    strf = 0;
%    currentLogFolder = DistrLogsFolder(EnterLogFolder).name;
%    strf = strfind(currentLogFolder, 'TestData');
    
%    if( strf > 0 )

%      newLogPath = [path2logs, '/',currentLogFolder]
      
      newLogPath = [path2logs];

      DistrConstFolders = dir(newLogPath);
      ConstFolderList = {DistrConstFolders.name};
      
      for EnterConstFolders=1:(numel(ConstFolderList))
        
        strf = 0;
        currentConstFolder = DistrConstFolders(EnterConstFolders).name;
        strf = strfind(currentConstFolder, 'DIST');
        
        if( strf > 0)
          newConstPath = [newLogPath, '/', currentConstFolder];

          maxCycleTime = 0;
          maxCycleTime = findMaxCycleTime(newConstPath);
          
          fopen([newConstPath,'/DistrMonitoring_',currentConstFolder,'.txt'],'w');
          
          DistrNodeFolders = dir(newConstPath);
          NodeFolderList = {DistrNodeFolders.name};
          
          
          for EnterNodeFolders=1:(numel(NodeFolderList))
            newFigure = 0;
            strf = 0;
            currentNodeFolder = DistrNodeFolders(EnterNodeFolders).name;
            strf = strfind(currentNodeFolder, 'Node');
            
              
            
            if( strf > 0 )
              
              Run = 0;
%              printf("Entering NodeFolder\n");
              newNodePath = [newConstPath, '/', currentNodeFolder];
              [NumberOfClusterMembers,ClusterMemberNames,isCH, isMasterCH, RunCount] = CountCM(newNodePath);
              
              if( RunCount > 0 )
                newFigure = 1;
              endif
              
              % 4 for [1..ClusterMemberName  2..AverageDuration  3..StandardDeviation 4..StandardError]
              CMDurationsOverNRuns = zeros(NumberOfClusterMembers,4);
              CMDurationsOverNRuns(:,1) = ClusterMemberNames;
              DurationRuns = zeros(NumberOfClusterMembers,RunCount); 
              CycleULDL = zeros(3, RunCount);
              
              % 3 for [ 1..CycleULDL Average  2..CycleULDL Deviation  3..CycleULDL Error]
              CycleULDLoverRuns = zeros(3,3);
              DistrRunFolders = dir(newNodePath);
              RunFolderList = {DistrRunFolders.name};
              
              for EnterRunFolder=1:(numel(RunFolderList))
                strf = 0;
                %TODO: create current Node as char for Printing
                currentRunFolder = DistrRunFolders(EnterRunFolder).name;
                strf = strfind(currentRunFolder, 'Run');
                  if( strf > 0 )
%                    printf("Entering Run Folder\n");
                    Run = Run + 1;
                    newRunPath = [newNodePath, '/', currentRunFolder];
                    [CylceTime, MCH_UL, CM_DL, CMDuration ] = analyseRunFolder(newRunPath, NumberOfClusterMembers);
                    CycleULDL(1,Run) = CylceTime;
                    CycleULDL(2,Run) = MCH_UL;
                    CycleULDL(3,Run) = CM_DL;
                    DurationRuns(:,Run) = CMDuration;
                  endif %strf Run
              endfor %EnterNodeFolders        
          
              if( RunCount ~= 0)      
              %CycleAverage
              CycleULDLoverRuns(1,1) = sum(CycleULDL(1,:))/RunCount;
              %CycleDeviation
              CycleULDLoverRuns(1,2) = std(CycleULDL(1,:));
              %CycleError
              CycleULDLoverRuns(1,3) = CycleULDLoverRuns(1,2)/sqrt(RunCount);
              
              %MCH_ULAverage
              CycleULDLoverRuns(2,1) = sum(CycleULDL(2,:))/RunCount;
              %MCH_ULDeviation
              CycleULDLoverRuns(2,2) = std(CycleULDL(2,:));
              %MCH_ULError
              CycleULDLoverRuns(2,3) = CycleULDLoverRuns(2,2)/sqrt(RunCount);
              
              %CM_DLAverage
              CycleULDLoverRuns(3,1) = sum(CycleULDL(3,:))/RunCount;
              %CM_DLDeviation
              CycleULDLoverRuns(3,2) = std(CycleULDL(3,:));
              %CM_DLError
              CycleULDLoverRuns(3,3) = CycleULDLoverRuns(3,2)/sqrt(RunCount);
          
                for i=1:NumberOfClusterMembers
                  CMDurationsOverNRuns(i,2) = sum(DurationRuns(i,:))/RunCount;
                  CMDurationsOverNRuns(i,3) = std(DurationRuns(i,:));
                  CMDurationsOverNRuns(i,4) = CMDurationsOverNRuns(i,3)/sqrt(RunCount);
                endfor
          
              endif
      

              CMDurationsOverNRuns = sortrows(CMDurationsOverNRuns, -2); 
              CMDurationsOverNRuns = CMDurationsOverNRuns/1000;
              CycleULDLoverRuns = CycleULDLoverRuns/1000;
               
              if( isCH == 1)

                if( isMasterCH == 0 )

                  xPositions = 0.5:0.5:(NumberOfClusterMembers+3)/2;
                  xTickLabels = {''};
                  xTickLabels(1,1) = "DL+UL";
                  xTickLabels(1,2) = 'MCH UL';
                  xTickLabels(1,3) = 'CM DL';
                   
                  for i=1:NumberOfClusterMembers
                    xTickLabels(1,i+3) = ['CM ', num2str(CMDurationsOverNRuns(i,1)*1000)];
                  endfor
                 
                  xTickLabelPosition = 4;
                  XLabels = char(xTickLabels);

                  if(newFigure == 1)
                  	printf("----------------------------------------------------------------\nNew Figure \n")
                    if(openFigures)
                      figure('name',currentConstFolder,'PaperType', 'a4','Visible','on','paperorientation','landscape');
                    else
                      figure('name',currentConstFolder,'PaperType', 'a4','Visible','off','paperorientation','landscape');
                    endif
                  endif

                  hold on;
                  errorbar( xPositions(1),CycleULDLoverRuns(1,1),CycleULDLoverRuns(1,3),'~obk');
                  bar( xPositions(1),CycleULDLoverRuns(1,1),'facecolor', [255 165 0]/255, 'edgecolor', [255 165 0]/255,(0.03*max(xPositions)));

                  errorbar( xPositions(2),CycleULDLoverRuns(2,1),CycleULDLoverRuns(2,3),'~obk');
                  bar( xPositions(2),CycleULDLoverRuns(2,1),'facecolor', [200 200 200]/255, 'edgecolor', [200 200 200]/255,(0.03*max(xPositions)));
  
                  errorbar( xPositions(3),CycleULDLoverRuns(3,1),CycleULDLoverRuns(3,3),'~obk');
                  bar( xPositions(3),CycleULDLoverRuns(3,1),'facecolor', [0 255 0]/255, 'edgecolor', [0 255 0]/255,(0.03*max(xPositions)));

                  hold off;


%xPositions(xTickLabelPosition:end),CMDurationsOverNRuns(:,2).',(CMDurationsOverNRuns(:,4).'),'ob',...

                  CMCount = 0;
                  for i=xTickLabelPosition:numel(xPositions)
                    CMCount = CMCount + 1;
                    hold on;
                    errorbar(xPositions(i),CMDurationsOverNRuns(CMCount,2).',(CMDurationsOverNRuns(CMCount,4).'),'~obk');
                    hold off;
                  endfor

                  CMCount = 0;
                  for i=xTickLabelPosition:numel(xPositions)
                    CMCount = CMCount + 1;
                    hold on;
                    bar(xPositions(i),CMDurationsOverNRuns(CMCount,2).','facecolor', [150 150 255]/255, 'edgecolor', [150 150 255]/255,(0.03*max(xPositions)));
                    hold off;
                  endfor

%                  set(h,'CapSize',12)

                  ax_pos=get(gca,'Position');

                  %shrink y-axis
                  set(gca,'Position',[ax_pos(1) ax_pos(2) ax_pos(3) ax_pos(4)]);
                  get(gca,'OuterPosition');
                  set(gca,'OuterPosition',[0 0.1 1 0.8])
                  set(gca, "fontsize", 16)
                  set(gca, 'xtick', xPositions) 
                  set(gca,'XTickLabel',{xTickLabels})
                  set(gca,'ylim',[0, dist_Max_Y_Axis_Border]);
                  set(gca,'xlim',[0.3, max(xPositions)+0.2]);

                  % Rotate the XTicks
                  h = get(gca, 'xlabel');
                  xlabelstring = get(h,'string');
                  xlabelposition = get(h,'position');
                  yposition = xlabelposition(2);
                  yposition = repmat(yposition,length(xPositions),1);
                  ylabel ("Duration in Seconds");
                  set(gca,'xtick',[]);
                  hnew = text(xPositions, 0.2*yposition, xTickLabels);
                  set(hnew,'rotation',90,'horizontalalignment','right','fontsize',16);
%                  set (gca, "xminorgrid", "on");
                  set (gca, "yminorgrid", "on");

                  hold on;
                  plot([0 (max(xPositions)+.2)],[dist_Max_Y_Axis_Border dist_Max_Y_Axis_Border],'-','color','black');
                  plot([(max(xPositions)+.2) (max(xPositions)+.2)],[0 dist_Max_Y_Axis_Border],'-','color','black');
                  
                  plot([0 numel(xPositions)+1],[maxCycleTime maxCycleTime],'--','color','red','linewidth',1.5); %waagrechte gerade 
                  text( max(xPositions), maxCycleTime-1, "Cycle", 'horizontalalignment', "center",'color','red' );
                  hold off;
                          
                          
                  %plots und textfile
                  if(createPlots)
                    printf("Graphics for %s getting created \n", currentConstFolder);
                    print ("-dsvg", [newConstPath, "/ErrorBars_", currentConstFolder,"_CH_",currentNodeFolder, ".svg"]);
                    set (gcf, 'paperorientation','landscape')
                    orient landscape;
                    print ("-dpdf", [newConstPath, "/ErrorBars_", currentConstFolder,"_CH_",currentNodeFolder, ".pdf"]);
                  endif
               
                  if(createTXT)
                    printf("Textfile for %s getting created \n", currentConstFolder);    
                    fileID = fopen([newConstPath,'/DistrMonitoring_',currentConstFolder,'.txt'],'a+');
                            
                    fprintf(fileID,'Distributed Monitoring Run-Series: %s \nCalculations over %d Runs \n\n', currentConstFolder, RunCount);
                    fprintf(fileID,'CH: %s\n_____________________________________________\n', currentNodeFolder);
                    fprintf(fileID,'ClusterMember \t\t Average \t\t Error \n');

                    for i=1:NumberOfClusterMembers
                      fprintf(fileID, 'CM %s: \t\t %s \t\t %s \n', num2str((CMDurationsOverNRuns(i,1).')*1000), num2str(CMDurationsOverNRuns(i,2).'),num2str(CMDurationsOverNRuns(i,4).'));
                    endfor

                    fprintf(fileID, '\nCycleAverage: \t %f \n', CycleULDLoverRuns(1,1) );
                    fprintf(fileID, 'CycleError: \t %f \n', CycleULDLoverRuns(1,3) );
                   
                    fprintf(fileID, '\nMCH-UL-Average: \t %f \n', CycleULDLoverRuns(2,1) );
                    fprintf(fileID, 'MCH-UL-Error: \t %f \n', CycleULDLoverRuns(2,3) );

                    fprintf(fileID, '\nCM-DL-Average: \t %f \n', CycleULDLoverRuns(3,1) );
                    fprintf(fileID, 'CM-DL-Error: \t %f \n\n', CycleULDLoverRuns(3,3) );
                    fclose(fileID);
                  endif
                endif
              endif
         
              if( isCH == 1)

                if( isMasterCH == 1 )
               
                  xPositions = 1:1:NumberOfClusterMembers+1;
                  xPositions = xPositions - 0.5;
                  xTickLabels = {''};
                  xTickLabels(1,1) = 'CM DL';

                  for i=1:NumberOfClusterMembers
                    xTickLabels(1,i+1) = ['CM ', num2str(CMDurationsOverNRuns(i,1)*1000)];
                  endfor
             
                  xTickLabelPosition = 2;
                  XLabels = char(xTickLabels);

                  if(newFigure == 1)
                    printf("----------------------------------------------------------------\nNew Figure \n")
                    if(openFigures)
                      figure('name',currentConstFolder,'PaperType', 'a4','Visible','on','paperorientation','landscape');
                    else
                      figure('name',currentConstFolder,'PaperType', 'a4','Visible','off','paperorientation','landscape');
                    endif
                  endif
                  
                  hold on;
                  errorbar( xPositions(1),CycleULDLoverRuns(3,1),CycleULDLoverRuns(3,3),'~obk' );
                  bar( xPositions(1),CycleULDLoverRuns(3,1),'facecolor', [0 255 0]/255, 'edgecolor', [0 255 0]/255,(0.03*max(xPositions)) );
                  hold off;

                  CMCount = 0;
                  for i=xTickLabelPosition:numel(xPositions)
                    CMCount = CMCount + 1;
                    hold on;
                    errorbar(xPositions(i),CMDurationsOverNRuns(CMCount,2).',(CMDurationsOverNRuns(CMCount,4).'),'~obk');
                    hold off;
                  endfor
                        
                  CMCount = 0;
                  for i=xTickLabelPosition:numel(xPositions)
                    CMCount = CMCount + 1;
                    hold on;
                    bar(xPositions(i),CMDurationsOverNRuns(CMCount,2).','facecolor', [150 150 255]/255, 'edgecolor', [150 150 255]/255,(0.03*max(xPositions)));
                    hold off;
                  endfor

                  get(gca,'OuterPosition');
                  set(gca,'OuterPosition',[0 0.1 1 0.8])
                  set(gca, "fontsize", 16)
                  set(gca, 'xtick', xPositions) 
                  set(gca,'XTickLabel',{xTickLabels})
                  set(gca,'ylim',[0, dist_Max_Y_Axis_Border]);
                  set(gca,'xlim',[0.3, max(xPositions)+0.2]);

                  % Rotate the XTicks
                  h = get(gca, 'xlabel');
                  xlabelstring = get(h,'string');
                  xlabelposition = get(h,'position');
                  yposition = xlabelposition(2);
                  yposition = repmat(yposition,length(xPositions),1);
                  ylabel ("Duration in Seconds");
                  set(gca,'xtick',[]);
                  hnew = text(xPositions, 0.2*yposition, xTickLabels);
                  set(hnew,'rotation',90,'horizontalalignment','right','fontsize',16);
%                  set (gca, "xminorgrid", "on");
                  set (gca, "yminorgrid", "on");

                  hold on;
                  plot([0 (max(xPositions)+.2)],[dist_Max_Y_Axis_Border dist_Max_Y_Axis_Border],'-','color','black');
                  plot([(max(xPositions)+.2) (max(xPositions)+.2)],[0 dist_Max_Y_Axis_Border],'-','color','black');
                  
                  plot([0 numel(xPositions)+1],[maxCycleTime maxCycleTime],'--','color','red','linewidth',1.5); %waagrechte gerade 
                  text( max(xPositions), maxCycleTime-1, "Cycle", 'horizontalalignment', "center",'color','red' )
                  hold off;
                    
                  if(createPlots)
                    printf("Graphics for %s getting created \n", currentConstFolder);
                    print ("-dsvg", [newConstPath, "/ErrorBars_", currentConstFolder,"_MCH_",currentNodeFolder, ".svg"]);
                    set (gcf, 'paperorientation','landscape')
                    orient landscape;
                    print ("-dpdf",[newConstPath, "/ErrorBars_", currentConstFolder,"_MCH_",currentNodeFolder, ".pdf"]);
                  endif
         
                  if(createTXT)
                    printf("Textfile for %s getting created \n", currentConstFolder);
                    fileID = fopen([newConstPath,'/DistrMonitoring_',currentConstFolder,'.txt'],'a+');
                    fprintf(fileID,'Distributed Monitoring Run-Series: %s \nCalculations over %d Runs \n\n', currentConstFolder, RunCount);
                    fprintf(fileID,'MCH: %s\n_____________________________________________\n', currentNodeFolder);
                    fprintf(fileID,'ClusterMember \t\t Average \t\t Error \n');

                    for i=1:NumberOfClusterMembers
                      fprintf(fileID, 'CM %s: \t\t %s \t\t %s \n', num2str((CMDurationsOverNRuns(i,1).')*1000), num2str(CMDurationsOverNRuns(i,2).'),num2str(CMDurationsOverNRuns(i,4).'));
                    endfor
                      
                    fprintf(fileID, '\nCM-DL-Average: \t %f \n', CycleULDLoverRuns(3,1) );
                    fprintf(fileID, 'CM-DL-Error: \t %f \n\n', CycleULDLoverRuns(3,3) );
                    fclose(fileID);
                  endif

                endif

              endif

            endif %strf Node

          endfor %EnterNodeFolders

        endif %strf Const

      endfor %EnterConstFolders

%    endif %strf TestData

%  endfor %EnterLogFolder 
 
endfor %ChooseFolder

