#! /usr/bin/octave -qf
clc
clear all
close all
more off
addpath ('functions');
config;

terminalOut = Show_Terminal_Outputs;
ShowFigures = Open_Created_Figures_In_Octave;
CreateTXT = Create_TXT_Summary;

set (0, "defaultaxesfontsize", 16) 
set (0, "defaulttextfontsize", 16) 

if(terminalOut)
  printf("--> 3_compare_monitoring.m was started \n \n");
endif

%Paths
Centr_MAN1_Path = Comp_Mon_Centr_MAN1_Path;
Centr_MAN15_Path = Comp_Mon_Centr_MAN15_Path;
Distr_SC_Path = Comp_Mon_Distr_SingChan_Path;
Distr_MC_Path = Comp_Mon_Distr_MultChan_Path;
Monitoring_Comparison_Output_Folder = Comp_Mon_Output_Folder;


%Reading TXT-Files
%=======================================


%Centr_MAN1
%---------------------------------------
CentrMAN1 = dir(Centr_MAN1_Path);
CentrMAN1FolderList = {CentrMAN1.name};

for Files=1:(numel(CentrMAN1FolderList))
  CurrentCentrMAN1PathPosition = CentrMAN1(Files).name;
  strf = strfind(CurrentCentrMAN1PathPosition, 'CentrMonitoring_');

  if( strf > 0 )
    strf = 0;
    if(terminalOut)
      printf("--> Found Centralized-Manager1-TXT-File and entering it \n\n");
    endif;
    CentrMAN1TxtFilePath = [Centr_MAN1_Path,'/', CurrentCentrMAN1PathPosition];

    %Opening Txt-File
    data = textread(CentrMAN1TxtFilePath,'%s','delimiter', '\n','whitespace','');

    %Running through the lines of the Txt-File
    for i=1:numel(data)
      Line = char(data(i,1));
      LineSplit = strsplit(Line);
      firstEntry = char(LineSplit(1,1));
      
      switch (firstEntry)
                
        case "CycleTimeAverage:"
          CentrMAN1CycleTimeAverageStrg = LineSplit(1,2);
          CentrMAN1CycleTimeAverageNum = CentrMAN1CycleTimeAverageStrg{1,1};
          CentrMAN1CycleTimeAverageNum = str2num(CentrMAN1CycleTimeAverageNum);
        
        case "CycleTimeError:"
          CentrMAN1CycleTimeErrorStrg = LineSplit(1,2);
          CentrMAN1CycleTimeErrorNum = CentrMAN1CycleTimeErrorStrg{1,1};
          CentrMAN1CycleTimeErrorNum = str2num(CentrMAN1CycleTimeErrorNum);

      endswitch
    endfor %running through Txt-File
  endif %strf
endfor %FolderList 


%Centr_MAN15
%---------------------------------------
CentrMAN15 = dir(Centr_MAN15_Path);
CentrMAN15FolderList = {CentrMAN15.name};

for Files=1:(numel(CentrMAN15FolderList))
  CurrentCentrMAN15PathPosition = CentrMAN15(Files).name;
  strf = strfind(CurrentCentrMAN15PathPosition, 'CentrMonitoring_');

  if( strf > 0 )
    strf = 0;
    if(terminalOut)
      printf("--> Found Centralized-Manager15-TXT-File and entering it \n\n");
    endif;
    CentrMAN15TxtFilePath = [Centr_MAN15_Path,'/', CurrentCentrMAN15PathPosition];

    %Opening Txt-File
    data = textread(CentrMAN15TxtFilePath,'%s','delimiter', '\n','whitespace','');

    %Running through the lines of the Txt-File
    for i=1:numel(data)
      Line = char(data(i,1));
      LineSplit = strsplit(Line);
      firstEntry = char(LineSplit(1,1));
      
      switch (firstEntry)
                
        case "CycleTimeAverage:"
          CentrMAN15CycleTimeAverageStrg = LineSplit(1,2);
          CentrMAN15CycleTimeAverageNum = CentrMAN15CycleTimeAverageStrg{1,1};
          CentrMAN15CycleTimeAverageNum = str2num(CentrMAN15CycleTimeAverageNum);
        
        case "CycleTimeError:"
          CentrMAN15CycleTimeErrorStrg = LineSplit(1,2);
          CentrMAN15CycleTimeErrorNum = CentrMAN15CycleTimeErrorStrg{1,1};
          CentrMAN15CycleTimeErrorNum = str2num(CentrMAN15CycleTimeErrorNum);

      endswitch
    endfor %running through Txt-File
  endif %strf
endfor %FolderList 


%Distr_SC
%---------------------------------------
DistrSC = dir(Distr_SC_Path);
DistrSCFolderList = {DistrSC.name};

for Files=1:(numel(DistrSCFolderList))
  CurrentDistrSCPathPosition = DistrSC(Files).name;
  strf = strfind(CurrentDistrSCPathPosition, 'DistrMonitoring_');

  if( strf > 0 )
    strf = 0;
    if(terminalOut)
      printf("--> Found Distributed-SingleChannel-TXT-File and entering it \n\n");
    endif;
    DistrSCTxtFilePath = [Distr_SC_Path,'/', CurrentDistrSCPathPosition];
    DistrSCxTickLabels = {''};
    DistrSCMCHULAverageValues = {''};
    DistrSCMCHULErrorValues = {''};
    DistrSCCMDLAverageValues = {''};
    DistrSCCMDLErrorValues = {''};
    SCCH = {''};
    SCMCH = {''};

    %Opening Txt-File
    data = textread(DistrSCTxtFilePath,'%s','delimiter', '\n','whitespace','');
    SCClusterHeadCounter = 0;
    SCCMDLAverageCounter = 0;
    SCCMDLErrorCounter = 0;
    DistrSCMCHCMDLTimeNum = 0;
    SCMaxCycleTime = 0;
    SCMaxCycleTimeError = 0;
    alsoTakeError = 0;
    NextIsMaster = 0;
    SCCHcount = 0;

    for i=1:numel(data)
      Line = char(data(i,1));
      LineSplit = strsplit(Line,{" ", "\f", "\n", "\r", "\t", "\v","_"});
      firstEntry = char(LineSplit(1,1));
%      printf("firstEntry: %s\n",firstEntry);
      
      switch( firstEntry )

        case "CH:"
          SCClusterHeadCounter = SCClusterHeadCounter + 1;
          SCCHcount = SCCHcount + 1;
          DistrSCxTickLabels(1,SCClusterHeadCounter) = [char(LineSplit(1,1)),char(LineSplit(1,3))];
          SCCH(SCCHcount,1) = char(LineSplit(1,3));

        case "MCH:"
          SCClusterHeadCounter = SCClusterHeadCounter + 1;
          DistrSCxTickLabels(1,SCClusterHeadCounter) = [char(LineSplit(1,1)),char(LineSplit(1,3))];

          SCMCH(1,1) = char(LineSplit(1,3));

          DistrSCMCHULAverageValues(1, SCClusterHeadCounter) = 0;
          NextIsMaster = 1;
        
        case "MCH-UL-Average:"
          DistrSCMCHULAverageValues(1, SCClusterHeadCounter) = char(LineSplit(1,2));
          SCCH(SCCHcount,4) = char(LineSplit(1,2));

        case "MCH-UL-Error:"
          SCCH(SCCHcount,5) = char(LineSplit(1,2));
          
        case "CM-DL-Average:"
          SCCMDLAverageCounter = SCCMDLAverageCounter + 1;
          DistrSCCMDLAverageValues(1,SCCMDLAverageCounter) = char(LineSplit(1,2));
          

          if( NextIsMaster )
            SCMCH(1,2) = char(LineSplit(1,2));

            DistrSCMCHCMDLTimeStrg = LineSplit(1,2);
            DistrSCMCHCMDLTimeNum = DistrSCMCHCMDLTimeStrg{1,1};
            DistrSCMCHCMDLTimeNum = str2num(DistrSCMCHCMDLTimeNum);
          endif

          if( ~NextIsMaster )
            SCCH(SCCHcount,2) = char(LineSplit(1,2));
          endif

        case "CM-DL-Error:"
          if( NextIsMaster )
            NextIsMaster = 0;

            SCMCH(1,3) = char(LineSplit(1,2));
          endif
          
          if( ~NextIsMaster )
            SCCH(SCCHcount,3) = char(LineSplit(1,2));
          endif

          SCCMDLErrorCounter = SCCMDLErrorCounter + 1;
          DistrSCCMDLErrorValues(1,SCCMDLErrorCounter) = char(LineSplit(1,2));

        case "CycleAverage:"
          SCCH(SCCHcount,6) = char(LineSplit(1,2));

          DistrSCCycleTimeAverageStrg = LineSplit(1,2);
          DistrSCCycleTimeAverageNum = DistrSCCycleTimeAverageStrg{1,1};
          DistrSCCycleTimeAverageNum = str2num(DistrSCCycleTimeAverageNum);
          if( DistrSCCycleTimeAverageNum > SCMaxCycleTime)
            SCMaxCycleTime = DistrSCCycleTimeAverageNum;
            alsoTakeError = 1;
          endif

        case "CycleError:"
          SCCH(SCCHcount,7) = char(LineSplit(1,2));

          DistrSCCycleTimeErrorStrg = LineSplit(1,2);
          DistrSCCycleTimeErrorNum = DistrSCCycleTimeErrorStrg{1,1};
          DistrSCCycleTimeErrorNum = str2num(DistrSCCycleTimeErrorNum);
          if( alsoTakeError )
            SCMaxCycleTimeError = DistrSCCycleTimeErrorNum;
            alsoTakeError = 0;
          endif

      endswitch
%      printf("LineName = %s\n",Line)
%      printf("FirstLine = %s \n", firstEntry);
      if(numel(LineSplit) > 1)
%        printf("NodeName = %s \n", char(LineSplit(1,2)));
      endif

    endfor

    if( DistrSCMCHCMDLTimeNum > SCMaxCycleTime )
      SCMaxCycleTime = DistrSCMCHCMDLTimeNum;
    endif

  endif
endfor

SCCHarray = zeros(numel(SCCH(:,1)),numel(SCCH(1,:)));
for CH = 1:numel(SCCH(:,1))
  for value = 1:numel(SCCH(1,:))
    SCCHarray(CH,value) = str2num(SCCH{CH,value});
  endfor
endfor
SCCHarray = sortrows (SCCHarray, 1);

SCMCHarray = zeros(numel(SCMCH(:,1)),numel(SCMCH(1,:)));
for MCH = 1:numel(SCMCH(:,1))
  for value = 1:numel(SCMCH(1,:))
    SCMCHarray(MCH,value) = str2num(SCMCH{MCH,value});
  endfor
endfor

%Distr_MC
%---------------------------------------
DistrMC = dir(Distr_MC_Path);
DistrMCFolderList = {DistrMC.name};

for Files=1:(numel(DistrMCFolderList))
  CurrentDistrMCPathPosition = DistrMC(Files).name;
  strf = strfind(CurrentDistrMCPathPosition, 'DistrMonitoring_');

  if( strf > 0 )
    strf = 0;
    if(terminalOut)
      printf("--> Found Distributed-MultiChannel-TXT-File and entering it \n\n");
    endif;
    DistrMCTxtFilePath = [Distr_MC_Path,'/', CurrentDistrMCPathPosition];
    DistrMCxTickLabels = {''};
    DistrMCCMDLAverageValues = {''};
    DistrMCCMDLErrorValues = {''};
    MCCH = {''};
    MCMCH = {''};

    %Opening Txt-File
    data = textread(DistrMCTxtFilePath,'%s','delimiter', '\n','whitespace','');
    MCClusterHeadCounter = 0;
    MCCMDLAverageCounter = 0;
    MCCMDLErrorCounter = 0;
    DistrMCMCHCMDLTimeNum = 0;
    MCMaxCycleTime = 0;
    MCMaxCycleTimeError = 0;
    alsoTakeError = 0;
    NextIsMaster = 0;
    MCCHcount = 0;

    for i=1:numel(data)
      Line = char(data(i,1));
      LineSplit = strsplit(Line,{" ", "\f", "\n", "\r", "\t", "\v","_"});
      firstEntry = char(LineSplit(1,1));
      
      switch( firstEntry )

        case "CH:"
          MCClusterHeadCounter = MCClusterHeadCounter + 1;
          MCCHcount = MCCHcount + 1;
          DistrMCxTickLabels(1,MCClusterHeadCounter) = [char(LineSplit(1,1)),char(LineSplit(1,3))];
          MCCH(MCCHcount,1) = char(LineSplit(1,3));

        case "MCH:"
          MCClusterHeadCounter = MCClusterHeadCounter + 1;
          DistrMCxTickLabels(1,MCClusterHeadCounter) = [char(LineSplit(1,1)),char(LineSplit(1,3))];

          MCMCH(1,1) = char(LineSplit(1,3));

          NextIsMaster = 1;

        case "MCH-UL-Average:"
          MCCH(MCCHcount,4) = char(LineSplit(1,2));

        case "MCH-UL-Error:"
          MCCH(MCCHcount,5) = char(LineSplit(1,2));

          
        case "CM-DL-Average:"
          MCCMDLAverageCounter = MCCMDLAverageCounter + 1;
          DistrMCCMDLAverageValues(1,MCCMDLAverageCounter) = char(LineSplit(1,2));
          

          if( NextIsMaster )
            MCMCH(1,2) = char(LineSplit(1,2));

            DistrMCMCHCMDLTimeStrg = LineSplit(1,2);
            DistrMCMCHCMDLTimeNum = DistrMCMCHCMDLTimeStrg{1,1};
            DistrMCMCHCMDLTimeNum = str2num(DistrMCMCHCMDLTimeNum);
          endif

          if( ~NextIsMaster )
            MCCH(MCCHcount,2) = char(LineSplit(1,2));
          endif

        case "CM-DL-Error:"
          if( NextIsMaster )
            NextIsMaster = 0;

            MCMCH(1,3) = char(LineSplit(1,2));
          endif
          
          if( ~NextIsMaster )
            MCCH(MCCHcount,3) = char(LineSplit(1,2));
          endif

          MCCMDLErrorCounter = MCCMDLErrorCounter + 1;
          DistrMCCMDLErrorValues(1,MCCMDLErrorCounter) = char(LineSplit(1,2));

        case "CycleAverage:"
          MCCH(MCCHcount,6) = char(LineSplit(1,2));

          DistrMCCycleTimeAverageStrg = LineSplit(1,2);
          DistrMCCycleTimeAverageNum = DistrMCCycleTimeAverageStrg{1,1};
          DistrMCCycleTimeAverageNum = str2num(DistrMCCycleTimeAverageNum);
          if( DistrMCCycleTimeAverageNum > MCMaxCycleTime)
            MCMaxCycleTime = DistrMCCycleTimeAverageNum;
            alsoTakeError = 1;
          endif

        case "CycleError:"
          MCCH(MCCHcount,7) = char(LineSplit(1,2));

          DistrMCCycleTimeErrorStrg = LineSplit(1,2);
          DistrMCCycleTimeErrorNum = DistrMCCycleTimeErrorStrg{1,1};
          DistrMCCycleTimeErrorNum = str2num(DistrMCCycleTimeErrorNum);
          if( alsoTakeError )
            MCMaxCycleTimeError = DistrMCCycleTimeErrorNum;
            alsoTakeError = 0;
          endif

      endswitch
%      printf("LineName = %s\n",Line)
%      printf("FirstLine = %s \n", firstEntry);
      if(numel(LineSplit) > 1)
%        printf("NodeName = %s \n", char(LineSplit(1,2)));
      endif

    endfor

    if( DistrMCMCHCMDLTimeNum > MCMaxCycleTime )
      MCMaxCycleTime = DistrMCMCHCMDLTimeNum;
    endif

  endif
endfor

MCCHarray = zeros(numel(MCCH(:,1)),numel(MCCH(1,:)));
for CH = 1:numel(MCCH(:,1))
  for value = 1:numel(MCCH(1,:))
    MCCHarray(CH,value) = str2num(MCCH{CH,value});
  endfor
endfor
MCCHarray = sortrows (MCCHarray, 1);

MCMCHarray = zeros(numel(MCMCH(:,1)),numel(MCMCH(1,:)));
for MCH = 1:numel(MCMCH(:,1))
  for value = 1:numel(MCMCH(1,:))
    MCMCHarray(MCH,value) = str2num(MCMCH{MCH,value});
  endfor
endfor


%Merging Plot-Data
%===================================================================

%Position-Labels
%-------------------------------------------------------------------
xPositions = .5:1:(2+1+numel(SCCHarray(:,1))+1+numel(MCCHarray(:,1)));
xPositionLabels = {''};
xPositionLabels(1,1) = "MCH\n1";
xPositionLabels(1,2) = "MCH\n15";

xPositionLabels(1,3) = ["MCH\n",num2str(SCMCHarray(1,1))];



for xTickLabelCount=1:numel(SCCHarray(:,1))
  xPositionLabels(1,3+xTickLabelCount) = ["CH\n",num2str(SCCHarray(xTickLabelCount,1))];
endfor

xPositionLabels(1,4+numel(SCCHarray(:,1))) = ["MCH\n",num2str(MCMCHarray(1,1))];


for xTickLabelCount=1:numel(MCCHarray(:,1))
  xPositionLabels(1,4+numel(SCCHarray(:,1))+xTickLabelCount) = ["CH\n",num2str(MCCHarray(xTickLabelCount,1))];
endfor
%-------------------------------------------------------------------

%Averages and Error Matrix for Errorbars
%-------------------------------------------------------------------
%AveErr = zeros((2+1+SCClusterHeadCounter+1+MCClusterHeadCounter),2); %initialization
%
%for AveErrPosition=1:(2+1+SCClusterHeadCounter+1+MCClusterHeadCounter)
%    
%  if( AveErrPosition == 1 )
%    AveErr(AveErrPosition,1) = CentrMAN1CycleTimeAverageNum;
%    AveErr(AveErrPosition,2) = CentrMAN1CycleTimeErrorNum;
%  endif
%
%  if( AveErrPosition == 2 )
%    AveErr(AveErrPosition,1) = CentrMAN15CycleTimeAverageNum;
%    AveErr(AveErrPosition,2) = CentrMAN15CycleTimeErrorNum;
%  endif
%
%  if( AveErrPosition == 3 )
%    AveErr(AveErrPosition,1) = SCMaxCycleTime;
%    AveErr(AveErrPosition,2) = SCMaxCycleTimeError;
%  endif
%
%  if( AveErrPosition > 3 )
%    if( AveErrPosition <= 3+SCClusterHeadCounter )
%      AveErr(AveErrPosition,1) = str2num(char(DistrSCCMDLAverageValues(1,AveErrPosition-3)));
%      AveErr(AveErrPosition,2) = str2num(char(DistrSCCMDLErrorValues(1,AveErrPosition-3)));
%    endif
%  endif
%
%  if( AveErrPosition == (2+1+SCClusterHeadCounter+1))
%    AveErr(AveErrPosition,1) = MCMaxCycleTime;
%    AveErr(AveErrPosition,2) = MCMaxCycleTimeError;
%  endif
%
%  if( AveErrPosition > (2+1+SCClusterHeadCounter+1))
%    if( AveErrPosition <= (2+1+SCClusterHeadCounter+1+MCClusterHeadCounter))
%      AveErr(AveErrPosition,1) = str2num(char(DistrMCCMDLAverageValues(1,(AveErrPosition-3-SCClusterHeadCounter-1))));
%      AveErr(AveErrPosition,2) = str2num(char(DistrMCCMDLErrorValues(1,(AveErrPosition-3-SCClusterHeadCounter-1)))); 
%    endif
%  endif
%
%endfor


%xPositionLabels = {"Cycle\nCentr.1", "Cycle\nCentr.15", "Cycle\nClust.SC", "CM-DL\n(Node10)", "CM-DL\n(Node15)", "CM-DL\n(Node20)", "CM-DL\n(Node22)", "CM-DL\n(Node8)", "Cycle\nClust.MC", "CM-DL\n(Node10)", "CM-DL\n(Node15)", "CM-DL\n(Node20)", "CM-DL\n(Node22)", "CM-DL\n(Node8)"};
%
%AveErr = [50.923667 , 1.554233 ; ...
%          27.044433 , 0.808187 ; ...
%          
%          58.188900 , 1.351144 ; ...
%          5.174633 , 0.121724 ; ...
%          9.0855 , 0.15519 ; ...
%          10.618233 , 0.130679 ; ...
%          10.0045 , 0.130532 ; ...
%          8.791 , 0.414859 ; ...
%          
%          47.025000 , 1.191006 ; ...
%          2.917633 , 0.139572 ; ...
%          4.4403 , 0.267938 ; ...
%          4.042 , 0.159406 ; ...
%          3.5864 , 0.160597 ; ...
%          3.325033 , 0.124922 ];

if( ShowFigures )
  figure('name',"compare_centrclust",'PaperType', 'a4','Visible','on','paperorientation','landscape','position',[0 0 2000 1500]);
elseif
  figure('name',"compare_centrclust",'PaperType', 'a4','Visible','off','paperorientation','landscape','position',[0 0 2000 1500]);
endif

hold on;

errorbar( %MCH 1 Centralized
          xPositions(1),CentrMAN1CycleTimeAverageNum,CentrMAN1CycleTimeErrorNum,"~obk")

reddot = plot([0 1],[CentrMAN1CycleTimeAverageNum CentrMAN1CycleTimeAverageNum],'--','color','red','linewidth',1.5);

cent1bar = bar( %MCH 1 Centralized
                xPositions(1),CentrMAN1CycleTimeAverageNum,"facecolor", "g", "edgecolor", "g",0.1);


errorbar( %MCH 15 Centralized
          xPositions(2),CentrMAN15CycleTimeAverageNum,CentrMAN15CycleTimeErrorNum,'~obk')

plot([1 2],[CentrMAN15CycleTimeAverageNum CentrMAN15CycleTimeAverageNum],'--','color','red','linewidth',1.5);

cent1bar = bar( %MCH 15 Centralized
                xPositions(2),CentrMAN15CycleTimeAverageNum,"facecolor", "g", "edgecolor", "g",0.1);


errorbar( %MCH SingleChannel
          xPositions(3),SCMCHarray(1,2),SCMCHarray(1,3),'~obk')

bar(xPositions(3),SCMCHarray(1,2),"facecolor", "g", "edgecolor", "g",0.1);
          
for i=1:numel(SCCHarray(:,1))
  errorbar( %CH SingleChannel CM-DL
            (xPositions(3+i)+0.15),SCCHarray(i,2), SCCHarray(i,3),'~obk')
endfor

for i=1:numel(SCCHarray(:,1))
  bar( %CH SingleChannel CM-DL
            (xPositions(3+i)+0.15),SCCHarray(i,2),"facecolor", "g", "edgecolor", "g",0.1)
endfor

for i=1:numel(SCCHarray(:,1))
  errorbar( %CH SingleChannel Cycle
            (xPositions(3+i)-0.15),SCCHarray(i,6), SCCHarray(i,7),'~obk')
endfor

for i=1:numel(SCCHarray(:,1))
  bar( %CH SingleChannel Cycle
            (xPositions(3+i)-0.15),SCCHarray(i,6),'facecolor', [255 165 0]/255, 'edgecolor', [255 165 0]/255,0.1)
endfor

plot([(xPositions(2)+0.5) (xPositions(3+SCCHcount)+0.5)],[max(SCCHarray(:,6)) max(SCCHarray(:,6))],'--','color','red','linewidth',1.5);
          
errorbar( %MCH MultiChannel
          xPositions(3+numel(SCCHarray(:,1))+1),MCMCHarray(1,2),MCMCHarray(1,3),'~obk')

bar(xPositions(3+numel(SCCHarray(:,1))+1),MCMCHarray(1,2),"facecolor", "g", "edgecolor", "g",0.1);


for i=1:numel(MCCHarray(:,1))
  errorbar( %CH MultiChannel CM-DL
            xPositions((3+numel(SCCHarray(:,1))+1)+i)+0.15,MCCHarray(i,2), MCCHarray(i,3),'~obk');
endfor

for i=1:numel(MCCHarray(:,1))
  cmdl = bar( %CH MultiChannel CM-DL
            xPositions((3+numel(SCCHarray(:,1))+1)+i)+0.15,MCCHarray(i,2),"facecolor", "g", "edgecolor", "g",0.1);
endfor

for i=1:numel(MCCHarray(:,1))
  errorbar( %CH MultiChannel Cycle
            xPositions((3+numel(SCCHarray(:,1))+1)+i)-0.15,MCCHarray(i,6), MCCHarray(i,7),'~obk');
endfor

for i=1:numel(MCCHarray(:,1))
  cycle = bar( %CH MultiChannel Cycle
            xPositions((3+numel(SCCHarray(:,1))+1)+i)-0.15,MCCHarray(i,6),'facecolor', [255 165 0]/255, 'edgecolor', [255 165 0]/255,0.1);
endfor

plot([(xPositions(3+SCCHcount)+0.5) (xPositions(numel(xPositions))+0.5)],[max(MCCHarray(:,6)) max(MCCHarray(:,6))],'--','color','red','linewidth',1.5);

hold off;

lgnd1 = legend([reddot cmdl cycle],'Cycle','CM DL','CM DL + MCH UL');
set( lgnd1, 'fontsize',16,'interpreter','latex','Position', [0.65 , 0.6 , 0.2 , 0.15] )

%erbar = errorbar( xPositions(1),CentrMAN1CycleTimeAverageNum,CentrMAN1CycleTimeErrorNum,'~or', ...
%          xPositions(2),CentrMAN15CycleTimeAverageNum,CentrMAN15CycleTimeErrorNum,'~or', ...
%          %MCH SingleChannel
%          xPositions(3),SCMCHarray(1,2),SCMCHarray(1,3),'~or', ...
%          %CH SingleChannel CM-DL
%          xPositions(4:(3+numel(SCCHarray(:,1)))),SCCHarray(1:(numel(SCCHarray(:,1))),2), SCCHarray(1:(numel(SCCHarray(:,1))),3),'~og', ...
%          %CH SingleChannel MCH-UL
%          xPositions(4:(3+numel(SCCHarray(:,1)))),SCCHarray(1:(numel(SCCHarray(:,1))),4)+SCCHarray(1:(numel(SCCHarray(:,1))),2), SCCHarray(1:(numel(SCCHarray(:,1))),5),'~or', ...
%          
%          %MCH MultiChannel
%          xPositions(3+numel(SCCHarray(:,1))+1),MCMCHarray(1,2),MCMCHarray(1,3),'~or', ...
%          %CH MultiChannel CM-DL
%          xPositions((3+numel(SCCHarray(:,1))+2):(3+numel(SCCHarray(:,1))+1+numel(MCCHarray(:,1)))),MCCHarray(1:(numel(MCCHarray(:,1))),2), MCCHarray(1:(numel(MCCHarray(:,1))),3),'~og', ...
%          %CH MultiChannel MCH-UL
%          xPositions((3+numel(SCCHarray(:,1))+2):(3+numel(SCCHarray(:,1))+1+numel(MCCHarray(:,1)))),MCCHarray(1:(numel(MCCHarray(:,1))),4)+MCCHarray(1:(numel(MCCHarray(:,1))),2), MCCHarray(1:(numel(SCCHarray(:,1))),5),'~or');
%



          
set(gca, 'xtick', xPositions) 
set(gca,'XTickLabel',{xPositionLabels})

                      limsy=get(gca,'ylim');
                      %printf( "first: %d  %d", limsy(1), limsy(2));
                      %set(gca,'ylim',[0, limsy(2)]);

                      set(gca,'ylim',[0, compare_Max_Y_Axis_Border]);
                      set(gca,'xlim',[0, numel(xPositions)]);

                      get(gca,'OuterPosition');
			                set(gca,'OuterPosition',[0 0.1 1 0.8])

                      ylabel ("Duration in Seconds");
                      % Rotate the XTicks
%                      h = get(gca, 'xlabel');
%                      xlabelstring = get(h,'string');
%                      xlabelposition = get(h,'position');
%                      yposition = xlabelposition(2);
%                      yposition = repmat(yposition,length(xPositions),1);
%
%                      
%
%                      set(gca,'xtick',[]);
%                      hnew = text(xPositions, 0.2*yposition, xPositionLabels);
%                      set(hnew,'rotation',90,'horizontalalignment','right');
%                      set (gca, "xminorgrid", "on");
                      set (gca, "yminorgrid", "on");

                      hold on;
                      plot([(xPositions(1)+0.5) (xPositions(1)+0.5)],[0 compare_Max_Y_Axis_Border-1],'--','color','black'); %waagerechte gerade
                      hold off;
                      
                      hold on;
                      text( (xPositions(1)+0.5) , (compare_Max_Y_Axis_Border-0.5) , "Centralized",'horizontalalignment', "center");
                      hold off;
                      
                      hold on;
                      plot([(xPositions(2)+0.5) (xPositions(2)+0.5)],[0 compare_Max_Y_Axis_Border],'--','color','black'); %waagerechte gerade
                      hold off;

                      hold on;
                      text( (2.5+(numel(SCCHarray(:,1))/2)) , (compare_Max_Y_Axis_Border-0.5) , "Distributed Single-Channel",'horizontalalignment', "center");
                      hold off;

                      hold on;
                      plot([(xPositions(3+SCCHcount)+0.5) (xPositions(3+SCCHcount)+0.5)],[0 compare_Max_Y_Axis_Border],'--','color','black'); %waagerechte gerade
                      hold off;

                      hold on;
                      text( (2.5+numel(SCCHarray(:,1))+1+(numel(SCCHarray(:,1))/2)) , (compare_Max_Y_Axis_Border-0.5) , "Distributed Multi-Channel",'horizontalalignment', "center");
                      hold off;

                      hold on;
                      plot([0 (xPositions(numel(xPositions))+0.5)],[compare_Max_Y_Axis_Border compare_Max_Y_Axis_Border],'-','color','black');
                      plot([0 (xPositions(numel(xPositions))+0.5)],[compare_Max_Y_Axis_Border-1 compare_Max_Y_Axis_Border-1],'-','color','black');
                      plot([(xPositions(numel(xPositions))+0.5) (xPositions(numel(xPositions))+0.5)],[0 compare_Max_Y_Axis_Border],'-','color','black');
                      hold off;

%h = legend("show") ;
[hleg, hleg_obj, hplot, labels] = legend ();
                      
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperPosition', [0 0 30 10]); %x_width=10cm y_width=15cm
%                      hold on;
%                      plot([0 numel(xPositions)+1],[CycleTimeAverage CycleTimeAverage],'--','color','red'); %waagerechte gerade
%                      hold off;
   
TimeStamp = strftime ("%d-%m-%y_%H-%M-%S", localtime(time()));

set (gcf, 'paperorientation','landscape')
orient landscape;                   
print ("-dsvg", [Monitoring_Comparison_Output_Folder,"/", "compare_monitoring_",TimeStamp, ".svg"]);
%set (gcf, 'paperorientation','landscape')
%orient landscape;
print ("-dpdf", [Monitoring_Comparison_Output_Folder,"/", "compare_monitoring_",TimeStamp, ".pdf"]);

if( CreateTXT )
  printf("Textfile for compare_monitoring_%s getting created \n", TimeStamp);
                      
  fileID = fopen([Monitoring_Comparison_Output_Folder,"/", "compare_monitoring_",TimeStamp,"_INFO",'.txt'],'w');
  fprintf(fileID,'This comparison is made up of the measurements in the following folders: \n\n');
  
  fprintf(fileID,'===========================================================================\n');
  fprintf(fileID,'For the Centralized case with Node 1 as Manager:\n');
  fprintf(fileID,'---------------------------------------------------------------------------\n');
  fprintf(fileID,'%s\n',Centr_MAN1_Path);
  fprintf(fileID,'===========================================================================\n');
  fprintf(fileID,'For the Centralized case with Node 15 as Manager:\n');
  fprintf(fileID,'---------------------------------------------------------------------------\n');
  fprintf(fileID,'%s\n',Centr_MAN15_Path);
  fprintf(fileID,'===========================================================================\n');
  fprintf(fileID,'For the Distributed case with Single-Channel:\n');
  fprintf(fileID,'---------------------------------------------------------------------------\n');
  fprintf(fileID,'%s\n',Distr_SC_Path);
  fprintf(fileID,'===========================================================================\n');
  fprintf(fileID,'For the Distributed case with Multi-Channel:\n');
  fprintf(fileID,'---------------------------------------------------------------------------\n');
  fprintf(fileID,'%s\n',Distr_MC_Path);
  fprintf(fileID,'===========================================================================\n');

  fclose(fileID);
endif


