#! /usr/bin/octave -qf
clc
clear all
close all
more off
addpath ('functions');
config;

  set(0,'defaulttextinterpreter','tex')
  set(0,'DefaultTextFontname', 'CMU Serif')
%  set(0,'DefaultTextFontname', 'Nimbus RomNo9L')
%  set(0,'DefaultAxesFontName', 'CMU Serif')

  %Switches
  terminalOut = Show_Terminal_Outputs; %adjust in config.m
  openFigures = Open_Created_Figures_In_Octave; %adjust in config.m
  createPlotFiles = Create_Graphic_Output_Files; %adjust in config.m
  
  if(terminalOut)
  printf("--> 0_clustering_eval.m was started \n \n");
  endif

  isReady = 0;

  Clustering_Evaluation_Path

  path2logs = Clustering_Evaluation_Path; %adjust in config.m
  
  Runs = runCount(path2logs);
  Durations = zeros(1,Runs);
  allClusterRunsInOneArray = zeros(70,70,Runs);
  NumberOfRun = 0;
  
  Adir = dir(path2logs); %save folder structure into a struct
  a = size(Adir);
  FolderList = {Adir.name}; %StringField to Char_array
  
 % RunFolder = blanks([]);
  
for xFolder=1:(numel(FolderList))
  ClusterNumber = 0;
  currentFolder = Adir(xFolder).name;
  isFolder = ~isletter(currentFolder);
  trueFolder = sum(isFolder(:) == 1);
  
  if (trueFolder >= 10) %NumberString with more than 10 positions 
  %printf("Entering Folder: %s \n", currentFolder);
  ClusteringDuration = 0;
  
  NumberOfRun = NumberOfRun + 1;
  
  cellRunFolder{NumberOfRun} = currentFolder;
  
  cf_for_filename = currentFolder;
  newpath = [path2logs, '/', currentFolder];
  Bdir = dir(newpath);
  FolderList1 = {Bdir.name};
  %printf("create Pic \n");
  for yFolder=1:(numel(FolderList1))
    
    strf = 0;
    
    currentFolder1 = Bdir(yFolder).name;
    strf = strfind(currentFolder1, 'Node');
    if(strf > 0)
      
      newpath1 = [newpath, '/', currentFolder1];
      Cdir = dir(newpath1);
      FolderList2 = {Cdir.name};
      
      
      for Files=1:(numel(FolderList2))
        currentFile = Cdir(Files).name;        
        
        if( strcmpi(currentFile, 'clustering.done') )
        
          data = textread([newpath1, '/clustering.done'], '%s','delimiter', '\n','whitespace','');    
            
            for i=1:numel(data)
              
              Line = char(data(i,1));
              LineSplit = strsplit(Line);
              firstEntry = char(LineSplit(1,1));
              
              switch (firstEntry)
                
                case "clusteringDuration:"
                  cDur = LineSplit(1,2);
                  cDurNum = cDur{1,1};
                  cDurNum = str2num(cDurNum);
                  if( cDurNum > ClusteringDuration )
                    ClusteringDuration = cDurNum;
                    Durations(1,NumberOfRun) = ClusteringDuration;
                   endif
                   
                case "myName:"
                  myName = LineSplit(1,2);
                  myNameChar = char(myName);
                  ClusterMember = myNameChar;
                  
                case "myMembership:"
                  Membership = char(LineSplit(1,2));
                  if (Membership == "CH")
                    isCH = 1;
                  else
                    isCH = 0;
                  endif
                  
                case "myClusterheadName:"
                  ClusterHead = char(LineSplit(1,2));
           
                case "myClusterChannel:"
                  cChan = LineSplit(1,2);
                  cChanNum = cChan{1,1};
                  cChanNum = str2num(cChanNum);
                  
                case "myCMList:"
                   clear ClusterMember;
                   CMList = LineSplit;
                   CMList(1,1) = myName;
                   ClusterMember = char(CMList);
                   CMStrCell = cellstr(ClusterMember(:,:));
                   CMString = strjoin(CMStrCell);

                case "myMasterName:"
                   Master = char(LineSplit(1,2));
                   
              endswitch
            endfor %numel(data)
            
              if(isCH)
                if(ClusterNumber == 0)
		  if(openFigures)
                  figure('name',cf_for_filename,'PaperType', 'a4','Visible','on','paperorientation','landscape');
		  else
		  figure('name',cf_for_filename,'PaperType', 'a4','Visible','off','paperorientation','landscape');
		  endif
                endif
                ClusterNumber = ClusterNumber + 1;
                CurrentCluster = RectCont(ClusterNumber, ClusterHead, Master, cChanNum, ClusterMember, newpath);
                allClusterRunsInOneArray(:,:,NumberOfRun) = allClusterRunsInOneArray(:,:,NumberOfRun) + CurrentCluster;
                clear CurrentCluster;
              endif
              
        else

        endif
        
       endfor
       
    endif
  endfor  
 

 RateInPercent = zeros(Runs,1);
 ConstellationNumber = zeros(Runs,1);
 variantCount = 0;

 
 for currentRun=1:Runs
   SameCluster = 0;
   ProzCluster = 0;
   MatchIndex = 0;
   
   
   for compareRun=1:Runs
     
	  ClusterSumTmp = allClusterRunsInOneArray(:,:,currentRun) - allClusterRunsInOneArray(:,:,compareRun);
		
     %ClusterSum = sum(allClusterRunsInOneArray(:,:,currentRun) - allClusterRunsInOneArray(:,:,compareRun));
	  
	  % in cases of equal number of ones and negative ones it is possible, that the algorithm shows qual clusters while they are different, therefor we inverse the negative ones. Hence we mark the different areas only with a 1.
	  for i=1:numel(ClusterSumTmp(:,1))
			for j=1:numel(ClusterSumTmp(1,:))
					if( ClusterSumTmp(i,j) == -1)
						ClusterSumTmp(i,j) = 1;
					endif
			endfor
	  endfor

	  ClusterSum = sum(ClusterSumTmp);
     
     if(ClusterSum == 0)
        ProzCluster = ProzCluster +1;
        if(currentRun > compareRun)
          SameCluster = SameCluster + 1;
        endif
     endif
     
     if(MatchIndex == 0)
      if(SameCluster == 1)
        MatchIndex = compareRun;
      endif
    endif
 
   endfor %compareRun
   
    if(MatchIndex ~= 0)
      ConstellationNumber(currentRun) = ConstellationNumber(MatchIndex);
    else
      variantCount = variantCount + 1;
      ConstellationNumber(currentRun) = variantCount;
    endif
    
    
    RateInPercent(currentRun) = ProzCluster/Runs*100;
    
 endfor %currentRun
   
 char_RateInPercent = num2str(RateInPercent);
 

 
 % Create output files...
  if(createPlotFiles)
  print ("-dsvg", [newpath, "/MeshConst_", cf_for_filename, ".svg"]);
  
  set (gcf, 'paperorientation','landscape')

  orient landscape;
  print ("-dpdf", [newpath, "/MeshConst_", cf_for_filename, ".pdf"]);

  if(terminalOut)
  printf("--> Plot for dataset %s was created \n \n", cf_for_filename);
  endif

  endif
  isReady = 1;
  %printf("Cluster n done \n");
  clear cf_for_filename;
  endif
  
endfor

RunFolder = char(cellRunFolder);
fileID = fopen([path2logs, '/', 'Cluster_Rate.txt'], 'w');
fprintf(fileID,'I found %d Clustering Runs in this Folder,\nthese are the rates of occurred Cluster-Constellations:\n\n',Runs);
SumDurations = 0;
AvgDurations = 0;
for i=1:Runs
fprintf(fileID,'%s :   %s%%  VN: %s  Duration: %d \n', RunFolder(i,:),char_RateInPercent(i,:),num2str(ConstellationNumber(i,:)),Durations(1,i));
SumDurations = SumDurations + Durations(1,i); 
endfor
AvgDurations = SumDurations / Runs;

fprintf(fileID,'\nNumber of different Cluster-Constellations: %d',max(ConstellationNumber));
fprintf(fileID,'\nAverage Clustering Duration: %d\n\n',AvgDurations);
fclose(fileID);

if(isReady)

if(terminalOut)
printf("--> All eligible pdf- and svg-Files were created \n \n");
printf("--> 0_clustering_eval.m ended without any problems \n \n");
endif

else
printf("Something went wrong!!! Quitting Application!!! \n");
endif

%for i=1:numel(ClusterMember(:,1))
%      ClusterMemberString(:,i) = gal2grid(ClusterMember(i,:));
%    endfor
%    ClusterMemberString = ClusterMemberString.';

