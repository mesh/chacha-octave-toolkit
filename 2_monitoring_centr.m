#! /usr/bin/octave -qf
clc
clear all
close all
more off
addpath ('functions');
config;

terminalOut = Show_Terminal_Outputs;
openFigures = Open_Created_Figures_In_Octave;
createOutputFiles = Create_Graphic_Output_Files;
CreateTXT = Create_TXT_Summary;

set (0, "defaultaxesfontsize", 16) 
set (0, "defaulttextfontsize", 16) 

if(terminalOut)
  printf("--> 2_monitoring_centr.m was started \n \n");
endif

%Testpath
%path2logs = '../../centralized/software/logs/OctaveTestFiles';
%Live Data Path
%path2logs = '../../centralized/software/logs';
%path2logs = '../../distributed/software/logs/2_monitoring/IF1only';

Monitoring_Centralized_Path
printf("\n----------------------------------------------------------------\n");

path2logs = Monitoring_Centralized_Path;
CentrLogsFolder = dir(path2logs);
LogFolderList = {CentrLogsFolder.name};

%for EnterLogFolder=1:(numel(LogFolderList))
%  strf = 0;
%  currentLogFolder = CentrLogsFolder(EnterLogFolder).name;
%  strf = strfind(currentLogFolder, 'TestData');
%  if( strf > 0 )
%  newLogPath = [path2logs, '/',currentLogFolder]

  newLogPath = path2logs;

  CentrMANFolders = dir(newLogPath);
  MANFolderList = {CentrMANFolders.name};

for EnterMANFolders=1:(numel(MANFolderList))
  
  FigureCreationCount = 0;
  
  strf = 0;
  currentMANFolder = CentrMANFolders(EnterMANFolders).name;
  
  strf = strfind(currentMANFolder, 'MAN');
  if( strf > 0 )
    %printf("MAN Folder Found \n");
    newMANPath = [newLogPath, '/', currentMANFolder]
    %printf("new Path is: %s \n", newRunPath);
    [RunCount, TimeFileCount] = CentrArrayCalc(newMANPath);

%    [RunCount, TimeFileCount] = CentrArrayCalc('../../../centralized/software/logs/TestData_250K/20180516_17-26-17_MAN1_MCS3_MaxProcs0_50Runs')

    CycleTimeVektor = zeros(1,RunCount);
    
    % 4 for [GALnr minTime maxTime Average]
    AllRunsAllTimes = zeros(TimeFileCount,2,RunCount);
    
    CentrNodeFolders = dir(newMANPath);
    NodeFolderList = {CentrNodeFolders.name};
    
    for EnterNodeFolders=1:(numel(NodeFolderList))
      
      FoundRuns = 0;
      FigureCreationCount = FigureCreationCount + 1;
      strf = 0;
      currentNodeFolder = CentrNodeFolders(EnterNodeFolders).name;
      strf = strfind(currentNodeFolder, 'Node');
      
      if( strf > 0 )
        %printf("Node Folder Found \n");
        newNodePath = [newMANPath, '/', currentNodeFolder];
          
        CentrRunFolders = dir(newNodePath);
        RunFolderList = {CentrRunFolders.name};
        Run = 0;
        for EnterRunFolders=1:(numel(RunFolderList))
          
          TimeStampStart = 0;
          TimeStampStartFlag = 0;
          TimeStampStop = 0;
          TimeStampStopFlag = 0;
          CycleTime = 0;
          
          
          strf = 0;
          currentRunFolder = CentrRunFolders(EnterRunFolders).name;
          strf = strfind(currentRunFolder, 'Run');
          
          if( strf > 0 )
            
            FoundRuns = 1;
            %NodeNameForPrinting = currentNodeFolder;
            Run = Run + 1;       
            %printf("Run Folder Found \n");
            newRunPath = [newNodePath, '/', currentRunFolder];
            %printf("new Path is: %s \n", newRunPath);
            CentrTimeFiles = dir(newRunPath);
            TimeFileList = {CentrTimeFiles.name};
            
            AllRunsAllTimes(:,:,Run) = CentrReadTimeFile(newRunPath, TimeFileCount);
%            AllRunsAllTimes(:,:,1) = CentrReadTimeFile(newRunPath, TimeFileCount);

            
            for EnterTimeStamps=1:(numel(TimeFileList))
              
                strf = 0;
                currentTimeStamp = CentrTimeFiles(EnterTimeStamps).name;
                strf = strfind(currentTimeStamp, 'timestamp_1');
              
                if( strf > 0)
                  TimeStampStartFlag = 1; 
                  TimeStartArray = strsplit(currentTimeStamp, {"timestamp","startClusterDataRetrieve", "_", "-"});
                  formatYearMonthDay = 'yyyymmdd';
                  Date2MilliSeconds = datenum(char(TimeStartArray(1,3)),formatYearMonthDay)*24*60*60*1000; %days from 01.01.0000 in ms
                  TimeStart =  Date2MilliSeconds + ...
                              str2num(TimeStartArray{1,4})*60*60*1000 + ...  %hours2ms
                              str2num(TimeStartArray{1,5})*60*1000 + ...     %min2ms
                              str2num(TimeStartArray{1,6})*1000 + ...        %sec2ms
                              str2num(TimeStartArray{1,7});                  %ms
                endif
                
                strf = 0;
                currentTimeStamp = CentrTimeFiles(EnterTimeStamps).name;
                strf = strfind(currentTimeStamp, 'timestamp_2');
              
                if( strf > 0)
                  TimeStampStopFlag = 1;
                  TimeStopArray = strsplit(currentTimeStamp, {"timestamp","endClusterDataRetrieve", "_", "-"});
                  formatYearMonthDay = 'yyyymmdd';
                  Date2MilliSeconds = datenum(char(TimeStopArray(1,3)),formatYearMonthDay)*24*60*60*1000; %days from 01.01.0000 in ms
                  TimeStop =  Date2MilliSeconds + ...
                              str2num(TimeStopArray{1,4})*60*60*1000 + ...  %hours2ms
                              str2num(TimeStopArray{1,5})*60*1000 + ...     %min2ms
                              str2num(TimeStopArray{1,6})*1000 + ...        %sec2ms
                              str2num(TimeStopArray{1,7});                  %ms
                endif
                
                if(TimeStampStartFlag == 1)
                  if(TimeStampStopFlag == 1)
                    CycleTime = TimeStop - TimeStart; %in ms
                  endif
                endif
            endfor

            CycleTimeVektor(1,Run) = CycleTime / 1000; %CycleTime in seconds      
            
          endif %strf Run
          
        endfor% Enter Run Folders
      endif
 

xPositionLabels = AllRunsAllTimes(:,1,1);
xPositionLabelsGAL = {''};
%xPositionLabelsGAL(1,1) = 'Cycle'; 

xPositions = 1:TimeFileCount+1;

AveragePositions = zeros(1,TimeFileCount);

MaxYPositions = zeros(1,TimeFileCount);
MinYPositions = zeros(1,TimeFileCount);

AllStandardDeviations = zeros(1,TimeFileCount);
AllStandardErrors = zeros(1,TimeFileCount);

CycleTimeDeviation = std(CycleTimeVektor);
CycleTimeError = CycleTimeDeviation/sqrt(RunCount);
CycleTimeAverage = sum(CycleTimeVektor)/RunCount;

for currentNode=1:numel(AveragePositions)
  YMax = 0;
  YMin = 0;
  currentNodeAverage = 0;
%  currentNode = 1;
  StandardDeviation = 0;
  StandardDeviationCurrentNode = zeros(1,RunCount);
  
  
  for currentRun=1:RunCount
    currentNodeAverage = currentNodeAverage + AllRunsAllTimes(currentNode,2,currentRun);
        
    if(YMax < AllRunsAllTimes(currentNode,2,currentRun))
      YMax = AllRunsAllTimes(currentNode,2,currentRun);
    endif
    
    if(YMin == 0)
      YMin = AllRunsAllTimes(currentNode,2,currentRun);
    endif
    
    if(YMin > AllRunsAllTimes(currentNode,2,currentRun))
      YMin = AllRunsAllTimes(currentNode,2,currentRun);
    endif
    
  endfor
  AveragePositions(1,currentNode) = currentNodeAverage/RunCount/1000;
  MaxYPositions(1,currentNode) = YMax/1000;
  MinYPositions(1,currentNode) = YMin/1000;
  
  StandardDeviationCurrentNode = AllRunsAllTimes(currentNode,2,:);
  StandardDeviation = std(StandardDeviationCurrentNode)/1000;
  AllStandardDeviations(1,currentNode) = StandardDeviation;
  AllStandardErrors(1,currentNode) = StandardDeviation/sqrt(RunCount);
  
endfor

          WholeAverageCalc = [xPositionLabels, AveragePositions.', MinYPositions.', MaxYPositions.', AllStandardErrors.'];
          WholeAverageCalc = sortrows (WholeAverageCalc, -2);
          
          xPositionLabelsGAL(1,1) = 'CM DL';
          
          for i=1:numel(xPositionLabels)
            
            xPositionLabelsGAL(1,i+1) = ['CM ', num2str(WholeAverageCalc(i,1))];
          endfor
          XLabels = char(xPositionLabelsGAL);

          if(FigureCreationCount == 1)
            printf("New Figure \n")
            if(openFigures)
              figure('name',currentMANFolder,'PaperType', 'a4','Visible','on','paperorientation','landscape','position',[0 0 2000 1500]);
            else
              figure('name',currentMANFolder,'PaperType', 'a4','Visible','off','paperorientation','landscape','position',[0 0 2000 1500]);
            endif
          endif
                      
                      if( FoundRuns )
                        hold on;

                        cycle = errorbar(1,CycleTimeAverage,CycleTimeError,'~obk');
  %                      set(cycle,'linewidth',1 )
                        bar(1,CycleTimeAverage,"facecolor", "g", "edgecolor", "g",0.1);

                        hold off;
                      
                     
                        
                        CMCount = 0;
                        for i=2:numel(xPositions)
                          CMCount = CMCount + 1;
                          hold on;
                          errorbar(xPositions(i),WholeAverageCalc(CMCount,2).',(WholeAverageCalc(CMCount,5).'),'~obk');
                          hold off;

                        endfor
            
                        CMCount = 0;
                        for i=2:numel(xPositions)
                          CMCount = CMCount + 1;
                          hold on;
                          bar(xPositions(i),WholeAverageCalc(CMCount,2).','facecolor', [150 150 255]/255, 'edgecolor', [150 150 255]/255,0.1);
                          hold off;
                        endfor

                       


                        set(gca, 'xtick', xPositions) 
                        set(gca,'XTickLabel',{xPositionLabelsGAL})


                        limsy=get(gca,'ylim');
                        %printf( "first: %d  %d", limsy(1), limsy(2));
                        %set(gca,'ylim',[0, limsy(2)]);
                        set(gca,'ylim',[0, cent_Max_Y_Axis_Border]);
                        set(gca,'xlim',[0, numel(xPositions)+1]);
						get(gca,'OuterPosition');
			            set(gca,'OuterPosition',[0 0.1 1 0.8])


                        %title(["Monitoring Centralized from Node", currentNodeFolder, "Run series: ", currentMANFolder]);
                        ylabel ("Duration in Seconds");

                        % Rotate the XTicks
                        h = get(gca, 'xlabel');
                        xlabelstring = get(h,'string');
                        xlabelposition = get(h,'position');
                        yposition = xlabelposition(2);
                        yposition = repmat(yposition,length(xPositions),1);



                        

                        set(gca,'xtick',[]);
                        hnew = text(xPositions, 0.2*yposition, xPositionLabelsGAL);
                        set(hnew,'rotation',90,'horizontalalignment','right');
  %                      set (gca, "xminorgrid", "on");
                        set (gca, "yminorgrid", "on");

  %                      hold on;
  %                      plot(xPositions(1),WholeAverageCalc(1,2),'s','MarkerSize',18, 'MarkerEdgeColor','red');
  %                      plot(xPositions(numel(xPositions)),WholeAverageCalc(numel(xPositions),2),'s','MarkerSize',18, 'MarkerEdgeColor','green');
  %                      hold off;
                        %grid on;
                        %grid minor on;
                        %set(xtickangle(90))
                        
                        hold on;
                        plot([0 (numel(xPositions)+1)],[cent_Max_Y_Axis_Border cent_Max_Y_Axis_Border],'-','color','black');
                        plot([(numel(xPositions)+1) (numel(xPositions)+1)],[0 cent_Max_Y_Axis_Border],'-','color','black');

                        plot([0 numel(xPositions)+1],[CycleTimeAverage CycleTimeAverage],'--','color','red','linewidth',1.5); %waagrechte gerade 
                        text( max(xPositions), CycleTimeAverage-.5, "Cycle", 'horizontalalignment', "right",'color','red' );
                        hold off;
                        
                        %printf("plot\n");
                      endif %Found Runs
              
     endfor %Enter Node Folders
       
   % Create output files...
                      if(createOutputFiles)
						set(gcf, 'PaperUnits', 'centimeters');
						set(gcf, 'PaperPosition', [0 0 30 10]); %x_width=10cm y_width=15cm
                      printf("Graphics for %s getting created \n", currentMANFolder);

                      set (gcf, 'paperorientation','landscape')
                      orient landscape;

                      print ("-dsvg", [newMANPath, "/ErrorBars_", currentMANFolder, ".svg"]);
                      
                      print ("-dpdf", [newMANPath, "/ErrorBars_", currentMANFolder, ".pdf"]);
                      endif

                      if(CreateTXT)
                        printf("Textfile for %s getting created \n", currentMANFolder);
                      
                        fileID = fopen([newMANPath,'/CentrMonitoring_',currentMANFolder,'.txt'],'w');
                        
                        fprintf(fileID,'Monitoring Run-Series: %s \nCalculations over %d Runs \n\n', currentMANFolder, RunCount);
                        fprintf(fileID,'NodeName \t\t Average \t\t MinTime \t\t MaxTime \t\t StandardDeviation \n');
                        for i=1:TimeFileCount
                          fprintf(fileID, 'Node %s:\t\t %s \t\t %s \t\t %s \t\t %s \n', num2str(WholeAverageCalc(i,1).'), num2str(WholeAverageCalc(i,2).'),num2str(WholeAverageCalc(i,3).'),num2str(WholeAverageCalc(i,4).'),num2str(WholeAverageCalc(i,5).'));
                        endfor
                        fprintf(fileID, '\n\nCycleTimeAverage: \t %f   \nCycleTimeError: \t %f   \nCycleTimeDeviation: \t %f \n', CycleTimeAverage, CycleTimeError,CycleTimeDeviation);
                        fclose(fileID);

                    endif 
  endif %strf MAN Folder
  
                    
endfor %Enter MAN Folder
%endif %strf Log Folder
%endfor %Enter Log Folder

