function [G] = gal2grid(GAL)

A=isletter(GAL); %gibt 1 und 0 raus, 1 bedeutet Buchstabe
B=GAL(~A); %alles was nicht Buchstabe ist 
P = str2num(B);

if (mod(P, 6) == 0)
  i = 6;
else
  i = mod(P, 6);
endif

G = [i ceil(P/6)];