function [] = plotall()

for i=1:6
  for j=1:6
    A(i, j) = j;
  endfor
endfor

x = 1:1:6;
y = 1:1:6;
[X,Y] = meshgrid(x,y);

marker_color = [0 0 0];
% Mesh-Grid plotten
plot(A,'o','MarkerSize',10,'MarkerEdgeColor',marker_color,'MarkerFaceColor',marker_color);
xlim([0 7]);
ylim([0 7]);