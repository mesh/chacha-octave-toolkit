  
  clc
  clear all
  close all
  PlotInner = 1;
  PlotOuter = 1;
  
  PlotMAN1 = 0;
  PlotMAN15 = 0;
  
  if(PlotInner)
  
  if(~PlotMAN1)
  if(~PlotMAN15)
  ClusterMemberString = ["GAL1"; "GAL2";"GAL3"; "GAL4";"GAL5";...
                         "GAL7"; "GAL8";"GAL9"; "GAL10";"GAL11";...
                         "GAL13"; "GAL14";"GAL15"; "GAL16";"GAL17";...
                         "GAL19"; "GAL20";"GAL21"; "GAL22";"GAL23";...
                         "GAL25"; "GAL26";"GAL27"; "GAL28";"GAL29"];
 endif
endif

   if(PlotMAN1)
  ClusterMemberString = [ "GAL2";"GAL3"; "GAL4";"GAL5";...
                         "GAL7"; "GAL8";"GAL9"; "GAL10";"GAL11";...
                         "GAL13"; "GAL14";"GAL15"; "GAL16";"GAL17";...
                         "GAL19"; "GAL20";"GAL21"; "GAL22";"GAL23";...
                         "GAL25"; "GAL26";"GAL27"; "GAL28";"GAL29"];
endif

   if(PlotMAN15)
  ClusterMemberString = [ "GAL1";"GAL2";"GAL3"; "GAL4";"GAL5";...
                         "GAL7"; "GAL8";"GAL9"; "GAL10";"GAL11";...
                         "GAL13"; "GAL14"; "GAL16";"GAL17";...
                         "GAL19"; "GAL20";"GAL21"; "GAL22";"GAL23";...
                         "GAL25"; "GAL26";"GAL27"; "GAL28";"GAL29"];
endif
                         
%  CMStringOuter = ["GAL6";"GAL12";"GAL18";"GAL24";"GAL30";"GAL31";"GAL32";"GAL33";"GAL34";"GAL35";"GAL36"];
%  CMString = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30  31 32 33 34 35 36];
  
  for i=1:numel(ClusterMemberString(:,1))
      ClusterMember(:,i) = gal2grid(ClusterMemberString(i,:));
    endfor
   
    Color = [0 0 0] ./255;
    CMtrans = ClusterMember.';
  
         nCM = numel(ClusterMember)/2; % number of ClusterMember /2 because there are x and y parameters for each CM
 hold on
  for i=1:nCM
        CMstring = ClusterMemberString(i,:);
        A=isletter(CMstring); %gibt 1 und 0 raus, 1 bedeutet Buchstabe
        B=CMstring(~A); %alles was nicht Buchstabe ist 
        
        plot(CMtrans(i,1),CMtrans(i,2),'o','MarkerSize',18,'MarkerEdgeColor',Color,'markerfacecolor',"none");
%        axis("off","square");

        text (CMtrans(i,1), CMtrans(i,2)+0.02, B,'horizontalalignment', "center",'interpreter',"latex");
%        PlotCM(ClusterMember(i,:),0,B, 0, ClusterNumber, num2str(ClusterChannel));
        %printf("x: %d, y: %d \n",ClusterMember(i,1),ClusterMember(i,2));
    endfor
endif      

if(PlotOuter)
      
CMStringOuter = ["GAL6";"GAL12";"GAL18";"GAL24";"GAL30";"GAL31";"GAL32";"GAL33";"GAL34";"GAL35";"GAL36"];
%  CMString = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30  31 32 33 34 35 36];
  for i=1:numel(CMStringOuter(:,1))
      CMOuter(:,i) = gal2grid(CMStringOuter(i,:));
    endfor
   
    Color = [200 200 200] ./255;
    CMtransOuter = CMOuter.';
  
         nCM = numel(CMOuter)/2; % number of ClusterMember /2 because there are x and y parameters for each CM
 hold on
  for i=1:nCM
        CMstring = CMStringOuter(i,:);
        A=isletter(CMstring); %gibt 1 und 0 raus, 1 bedeutet Buchstabe
        B=CMstring(~A); %alles was nicht Buchstabe ist 
        
        plot(CMtransOuter(i,1),CMtransOuter(i,2),'o','MarkerSize',18,'MarkerEdgeColor',Color,'markerfacecolor',"none");
%        axis("off","square");

        text(CMtransOuter(i,1), CMtransOuter(i,2)+0.02, B, 'color', Color,'horizontalalignment', "center",'interpreter',"latex");
%        PlotCM(ClusterMember(i,:),0,B, 0, ClusterNumber, num2str(ClusterChannel));
        %printf("x: %d, y: %d \n",ClusterMember(i,1),ClusterMember(i,2));
    endfor
    
 endif
 
 if(PlotMAN1)
      
CMStringMAN = ["GAL1"];
  for i=1:numel(CMStringMAN(:,1))
      CMMAN(:,i) = gal2grid(CMStringMAN(i,:));
    endfor
   
    Color = [200 200 200] ./255;
    CMtransMAN = CMMAN.';
  
         nCM = numel(CMMAN)/2; % number of ClusterMember /2 because there are x and y parameters for each CM
 hold on
  for i=1:nCM
        CMstring = CMStringMAN(i,:);
        A=isletter(CMstring); %gibt 1 und 0 raus, 1 bedeutet Buchstabe
        B=CMstring(~A); %alles was nicht Buchstabe ist 
        
        plot(CMtransMAN(i,1),CMtransMAN(i,2),'o','MarkerSize',18,'MarkerEdgeColor','r','markerfacecolor',"none");
        plot(CMtransMAN(i,1),CMtransMAN(i,2),'o','MarkerSize',26,'MarkerEdgeColor','r','markerfacecolor',"none");
        
        text (CMtransMAN(i,1), CMtransMAN(i,2)+0.02, B,'horizontalalignment', "center",'interpreter',"latex");


    endfor
    
 endif
 
  if(PlotMAN15)
      
CMStringMAN = ["GAL15"];
  for i=1:numel(CMStringMAN(:,1))
      CMMAN(:,i) = gal2grid(CMStringMAN(i,:));
    endfor
   
    Color = [200 200 200] ./255;
    CMtransMAN = CMMAN.';
  
         nCM = numel(CMMAN)/2; % number of ClusterMember /2 because there are x and y parameters for each CM
 hold on
  for i=1:nCM
        CMstring = CMStringMAN(i,:);
        A=isletter(CMstring); %gibt 1 und 0 raus, 1 bedeutet Buchstabe
        B=CMstring(~A); %alles was nicht Buchstabe ist 
               
        plot(CMtransMAN(i,1),CMtransMAN(i,2),'o','MarkerSize',18,'MarkerEdgeColor','r','markerfacecolor',"none");
        plot(CMtransMAN(i,1),CMtransMAN(i,2),'o','MarkerSize',26,'MarkerEdgeColor','r','markerfacecolor',"none");
        
        text (CMtransMAN(i,1), CMtransMAN(i,2)+0.02, B,'horizontalalignment', "center",'interpreter',"latex");


    endfor
    
 endif
 
 hold off
 axis("off","square");
 
 if(PlotOuter)
  %print ("-dpng", ["../PlotsNoClusters/", "WholeCluster", ".png"]);
  print ("-dsvg", ["../PlotsNoClusters/", "WholeCluster", ".svg"]);
  print ("-dpdf", ["../PlotsNoClusters/", "WholeCluster", ".pdf"]);
endif
 
 if(PlotMAN1)
  print ("-dsvg", ["../PlotsNoClusters/", "ZentrMAN1", ".svg"]);
  print ("-dpdf", ["../PlotsNoClusters/", "ZentrMAN1", ".pdf"]);
endif
 
 if(PlotMAN15)
  print ("-dsvg", ["../PlotsNoClusters/", "ZentrMAN15", ".svg"]);
  print ("-dpdf", ["../PlotsNoClusters/", "ZentrMAN15", ".pdf"]);
endif


  
  
  
  
  


