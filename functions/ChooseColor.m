function [Color] = ChooseColor(ColorNumber)

%    switch(ColorNumber)
%      case 1 
%        Color = [65 105 225] ./255; %Blau
%      case 2
%        Color = [255 0 0] ./255; %Rot
%      case 3
%        Color = [0 255 0] ./255; %Grün 
%      case 4
%        Color = [255 193 37] ./255; %Gelb
%      case 5
%        Color = [255 20 147] ./255; %Magenta
%	     case 6
%        Color = [165 42 42] ./255; %Brown
%    endswitch

% choose color coupled with channelNumber
    switch(ColorNumber)
      case 36 
        Color = [65 105 225] ./255; %Blau
      case 40
        Color = [255 0 0] ./255; %Rot
      case 44
        Color = [0 255 0] ./255; %Grün 
      case 48
        Color = [255 193 37] ./255; %Gelb
      case 153
        Color = [255 20 147] ./255; %Magenta
	    case 157
        Color = [165 42 42] ./255; %Brown
    endswitch
