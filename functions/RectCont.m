function [A] = RectCont(ClusterNumber, ClusterHeadString,Master, ClusterChannel, ClusterMemberString, Path)
  
    hold on;
    ClusterHead = gal2grid(ClusterHeadString); 
   
    for i=1:numel(ClusterMemberString(:,1))
      ClusterMember(:,i) = gal2grid(ClusterMemberString(i,:));
    endfor
    ClusterMember = ClusterMember.';
    
    printMaster = gal2grid(Master);
    
    LargeRect = 4;
    nCM = numel(ClusterMember)/2; % number of ClusterMember /2 because there are x and y parameters for each CM
    meshx = 0.1:.1:7; %// x axis
    meshy = 0.1:.1:7; %// y axis
    [X Y] = meshgrid(meshx,meshy); %// all combinations of x, y
    F = zeros(numel(meshx), numel(meshy));
    A = zeros(numel(meshx), numel(meshy));
    
    % Print Clustermember and ClusterHead
    %ClusterHead
    CHstring = ClusterHeadString;
    A=isletter(CHstring); %gibt 1 und 0 raus, 1 bedeutet Buchstabe
    B=CHstring(~A); %alles was nicht Buchstabe ist 
    %ClusterHead
    PlotCM(ClusterHead,0,B, 1, ClusterNumber,num2str(ClusterChannel));
    
    %Master
    PlotCM( printMaster,1, "trivial", 1, ClusterNumber,num2str(ClusterChannel));
    
    
    %ClusterMember
    for i=1:nCM
        CMstring = ClusterMemberString(i,:);
        A=isletter(CMstring); %gibt 1 und 0 raus, 1 bedeutet Buchstabe
        B=CMstring(~A); %alles was nicht Buchstabe ist 
        
        PlotCM(ClusterMember(i,:),0,B, 0, ClusterNumber, num2str(ClusterChannel));
        %printf("x: %d, y: %d \n",ClusterMember(i,1),ClusterMember(i,2));
    endfor
      
    
    
    % Draw Ractangulars
     for i=1:nCM
          xRectStart = 10*ClusterMember(i,2)-LargeRect;
          xRectStop = 10*ClusterMember(i,2)+LargeRect;
          yRectStart = 10*ClusterMember(i,1)-LargeRect;
          yRectStop = 10*ClusterMember(i,1)+LargeRect;
          
          
       for j=xRectStart:1:xRectStop
         for k=yRectStart:1:yRectStop  
           F(j,k) = ClusterChannel;
         endfor
       endfor
     endfor
     
    % Fill Spaces
    xFillStart = 1;
    xFillStop = numel(meshy);
    yFillStart = 1;
    yFillStop = numel(meshx);
    
for yLaufVar=yFillStart:1:yFillStop
  for xLaufVar = xFillStart:1:xFillStop-5
    
    if( F(xLaufVar  ,yLaufVar) ~=0 )
    %printf("i is not 0 \n");
    
    if( F(xLaufVar+1,yLaufVar) ==0 )
    %printf("i+1 is 0 \n");

    if( F(xLaufVar+5,yLaufVar) ~=0 )
    %printf("i+5 is not 0 \n");
    
     F(xLaufVar+1,yLaufVar) = ClusterChannel;
     F(xLaufVar+2,yLaufVar) = ClusterChannel;
    
    endif
    endif
    endif
  endfor
endfor
      
for xLaufVar=xFillStart:1:xFillStop
  for yLaufVar = yFillStart:1:yFillStop-5
    
    if( F(xLaufVar  ,yLaufVar) ~=0 )
    %printf("i is not 0 \n");
    
    if( F(xLaufVar  ,yLaufVar+1) ==0 )
    %printf("i+1 is 0 \n");

    if( F(xLaufVar  ,yLaufVar+5) ~=0 )
    %printf("i+5 is not 0 \n");
    
     F(xLaufVar,yLaufVar+1) = ClusterChannel;
     F(xLaufVar,yLaufVar+1) = ClusterChannel;
    
    endif
    endif
    endif
  endfor
endfor

     A = F/ClusterChannel;
     
%Color = ChooseColor(ClusterNumber);
Color = ChooseColor(ClusterChannel);
 
v = [ClusterChannel,ClusterChannel];
[c, h] = contour(X,Y,F,v,'linecolor',Color,'linewidth',2); 

axis("off","square");

hold off;    