function [RunCount, TimeFileCount] = CentrArrayCalc(MANPath)

CentrNodeFolders = dir(MANPath);
NodeFolderList = {CentrNodeFolders.name};
    
for EnterNodeFolders=1:(numel(NodeFolderList))
  
  strf = 0;
  currentNodeFolder = CentrNodeFolders(EnterNodeFolders).name;
  strf = strfind(currentNodeFolder, 'Node');
  
  if( strf > 0 )
    %printf("Node Folder Found \n");
    newNodePath = [MANPath, '/', currentNodeFolder];
      
    CentrRunFolders = dir(newNodePath);
    RunFolderList = {CentrRunFolders.name};
    
    for EnterRunFolders=1:(numel(RunFolderList))
      
      strf = 0;
      currentRunFolder = CentrRunFolders(EnterRunFolders).name;
      strf = strfind(currentRunFolder, 'Run');
      
      if( strf > 0 )
        
        % Count included Runs
        RunCount = CentrRunCount(newNodePath);
      
        %printf("Run Folder Found \n");
        newRunPath = [newNodePath, '/', currentRunFolder];
        %printf("new Path is: %s \n", newRunPath);
        CentrTimeFiles = dir(newRunPath);
        TimeFileList = {CentrTimeFiles.name};
        
%        TimeFileCount = CentrTimeFileCount('../../../centralized/software/logs/TestData_250K/20180516_17-26-17_MAN1_MCS3_MaxProcs0_50Runs/Node_1/Run_1')

        % +1 for the center Node itselv (GAL1 or GAL15)
        TimeFileCount = CentrTimeFileCount(newRunPath); 
        
        
      endif
    endfor
  endif   
endfor