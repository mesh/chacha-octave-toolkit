function [maxCycleTime] = findMaxCycleTime(newConstPath)

%newConstPath = '/home/meshlab1/Resilio Sync Tim Brockmann/CHaChA/distributed/software/logs/2_monitoring/20180411_15-06-33_DIST_MultiChan_Const4_250K_MCS0_IF2-ASUS_50Runs';

printf("Detecting Max Cycle Time\n");

DistrNodeFolders = dir(newConstPath);
NodeFolderList = {DistrNodeFolders.name};
maxCycleTime = 0;


for EnterNodeFolders=1:(numel(NodeFolderList))

  CurrentNodeCycleTimeOverAllRuns = 0;
  strf = 0;
  currentNodeFolder = DistrNodeFolders(EnterNodeFolders).name;
  strf = strfind(currentNodeFolder, 'Node');
  
  if( strf > 0 )
    
    Run = 0;
    RunCount = 0;
%    printf("%s\n", currentNodeFolder);

%    printf("Entering NodeFolder\n");
    newNodePath = [newConstPath, '/', currentNodeFolder];
    [NumberOfClusterMembers,ClusterMemberNames,isCH, isMasterCH, RunCount] = CountCM(newNodePath);
    
    CycleULDL = zeros(3, RunCount);
    
    % 3 for [ 1..CycleULDL Average  2..CycleULDL Deviation  3..CycleULDL Error]
    CycleULDLoverRuns = zeros(3,3);
    DistrRunFolders = dir(newNodePath);
    RunFolderList = {DistrRunFolders.name};

    for EnterRunFolder=1:(numel(RunFolderList))
      strf = 0;
      %TODO: create current Node as char for Printing
      currentRunFolder = DistrRunFolders(EnterRunFolder).name;
      strf = strfind(currentRunFolder, 'Run');
        if( strf > 0 )
%          printf("Entering Run Folder\n");
          Run = Run + 1;
          newRunPath = [newNodePath, '/', currentRunFolder];
          [CylceTime, MCH_UL, CM_DL, CMDuration ] = analyseRunFolder(newRunPath, NumberOfClusterMembers);
          CycleULDL(1,Run) = CylceTime;
%          printf( "%d\n",CylceTime);
          CycleULDL(3,Run) = CM_DL;

        endif %strf Run
    endfor %EnterNodeFolders
    
    if( RunCount > 0 )

      avgTimeOverAllRuns = sum((CycleULDL(1,:)))/RunCount;
%      printf("avgTime: %d\n", avgTimeOverAllRuns);

      if( avgTimeOverAllRuns > 0)
        CurrentNodeCycleTimeOverAllRuns = avgTimeOverAllRuns/1000;
      else
        CurrentNodeCycleTimeOverAllRuns = (sum((CycleULDL(3,:)))/RunCount)/1000;
      endif

    endif

%    if( CurrentNodeCycleTimeOverAllRuns > 0 )
%      printf("current node cycle Time: %d\n", CurrentNodeCycleTimeOverAllRuns);
%    endif

  endif

  if( CurrentNodeCycleTimeOverAllRuns > maxCycleTime )
    maxCycleTime = CurrentNodeCycleTimeOverAllRuns;

%    if( maxCycleTime > 0 )
%      printf("current max cycle Time: %d\n", maxCycleTime);
%    endif
  endif 
      


endfor

printf("Current max cycle Time: %d\n", maxCycleTime);

