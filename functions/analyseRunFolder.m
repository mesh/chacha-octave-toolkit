function [CylceTime, DataUpload, DataRetrieve, CMDuration] = analyseRunFolder(newRunPath, NumberOfClusterMembers)
  
  DistrRunFolderContent = dir(newRunPath);
  RunFolderContentList = {DistrRunFolderContent.name};
  GALCounter = 0;
  TS1Flag = 0;
  TS2Flag = 0;
  TS3Flag = 0;
  TS4Flag = 0;
  StartRetr = 0;
  EndRetr = 0;
  StartUpload = 0;
  EndUpload = 0;
  CylceTime = 0;
  DataUpload = 0;
  DataRetrieve = 0;
  CMDuration = zeros(NumberOfClusterMembers,1);
  
  for ContentPosition=1:(numel(RunFolderContentList))
    strf = 0;
    currentContent = DistrRunFolderContent(ContentPosition).name;
    
    strf = strfind(currentContent, 'time_netcat_node');
    if( strf > 0 )
      GALCounter = GALCounter + 1;
        data = textread([newRunPath,'/',currentContent],'%s','delimiter', '\n','whitespace','');
            
            for i=1:numel(data)
                
              Line = char(data(i,1));
              LineSplit = strsplit(Line);
              firstEntry = char(LineSplit(1,1));
              
              switch(firstEntry)
                case "real"
                  RealDurationLine = strsplit(Line, {"real","  ","m","s"});
                  min2millisec = str2num(char(RealDurationLine(1,2)))*60*1000;
                  sec2millisec = str2num(char(RealDurationLine(1,3)))*1000;
                  overallmillisec = min2millisec + sec2millisec;
                  CMDuration(GALCounter,1) = overallmillisec;
              endswitch

            endfor  
    endif
    
    
    

    strf = 0;
    strf = strfind(currentContent, 'timestamp_1');
    if( strf > 0 )
      TS1Flag = 1;
      TimeArray = strsplit(currentContent, {"timestamp","startClusterDataRetrieve", "_", "-"});
                  formatYearMonthDay = 'yyyymmdd';
                  Date2MilliSeconds = datenum(char(TimeArray(1,3)),formatYearMonthDay)*24*60*60*1000; %days from 01.01.0000 in ms
                  StartRetr =  Date2MilliSeconds + ...
                              str2num(TimeArray{1,4})*60*60*1000 + ...  %hours2ms
                              str2num(TimeArray{1,5})*60*1000 + ...     %min2ms
                              str2num(TimeArray{1,6})*1000 + ...        %sec2ms
                              str2num(TimeArray{1,7});                  %ms
      clear TimeArray
    endif
    strf = 0;
    strf = strfind(currentContent, 'timestamp_2');
    if( strf > 0 )
      TS2Flag = 1;
      TimeArray = strsplit(currentContent, {"timestamp","endClusterDataRetrieve", "_", "-"});
                  formatYearMonthDay = 'yyyymmdd';
                  Date2MilliSeconds = datenum(char(TimeArray(1,3)),formatYearMonthDay)*24*60*60*1000; %days from 01.01.0000 in ms
                  EndRetr =  Date2MilliSeconds + ...
                              str2num(TimeArray{1,4})*60*60*1000 + ...  %hours2ms
                              str2num(TimeArray{1,5})*60*1000 + ...     %min2ms
                              str2num(TimeArray{1,6})*1000 + ...        %sec2ms
                              str2num(TimeArray{1,7});                  %ms
      clear TimeArray
    endif
    strf = 0;
    strf = strfind(currentContent, 'timestamp_3');
    if( strf > 0 )
      TS3Flag = 1;
      TimeArray = strsplit(currentContent, {"timestamp","startClusterDataUpload", "_", "-"});
                  formatYearMonthDay = 'yyyymmdd';
                  Date2MilliSeconds = datenum(char(TimeArray(1,3)),formatYearMonthDay)*24*60*60*1000; %days from 01.01.0000 in ms
                  StartUpload =  Date2MilliSeconds + ...
                              str2num(TimeArray{1,4})*60*60*1000 + ...  %hours2ms
                              str2num(TimeArray{1,5})*60*1000 + ...     %min2ms
                              str2num(TimeArray{1,6})*1000 + ...        %sec2ms
                              str2num(TimeArray{1,7});                  %ms
      clear TimeArray
    endif
    strf = 0;
    strf = strfind(currentContent, 'timestamp_4');
    if( strf > 0 )
      TS4Flag = 1;
      TimeArray = strsplit(currentContent, {"timestamp","endClusterDataUpload", "_", "-"});
                  formatYearMonthDay = 'yyyymmdd';
                  Date2MilliSeconds = datenum(char(TimeArray(1,3)),formatYearMonthDay)*24*60*60*1000; %days from 01.01.0000 in ms
                  EndUpload =  Date2MilliSeconds + ...
                              str2num(TimeArray{1,4})*60*60*1000 + ...  %hours2ms
                              str2num(TimeArray{1,5})*60*1000 + ...     %min2ms
                              str2num(TimeArray{1,6})*1000 + ...        %sec2ms
                              str2num(TimeArray{1,7});                  %ms
      clear TimeArray
    endif
  endfor

  if( TS4Flag )
    if( TS1Flag )
      CylceTime = EndUpload - StartRetr;
    endif
  endif

  DataUpload = EndUpload - StartUpload;
  DataRetrieve = EndRetr - StartRetr;
  