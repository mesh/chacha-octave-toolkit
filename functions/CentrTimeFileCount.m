function [TimeFileCount] = CentrTimeFileCount(RunPath)
  
        CentrTimeFiles = dir(RunPath);
        TimeFileList = {CentrTimeFiles.name};
        
        TimeFileCount = 0;
        
        for EnterTimeFiles=1:(numel(TimeFileList))
          
          %printf("any file\n");
          strf = 0;
          currentTimeFile = CentrTimeFiles(EnterTimeFiles).name;
          strf = strfind(currentTimeFile, 'time_netcat');
          
          if( strf > 0 )
            
            %printf("time file\n");
            TimeFileCount = TimeFileCount + 1;
            
          endif
        
        endfor