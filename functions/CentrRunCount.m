function [RunCount] = CentrRunCount(NodePath)
        
        CentrRunFolders = dir(NodePath);
        RunFolderList = {CentrRunFolders.name};
        
        RunCount = 0;
        
        for EnterRunFolders=1:(numel(RunFolderList))
          
          strf = 0;
          currentRunFolder = CentrRunFolders(EnterRunFolders).name;
          strf = strfind(currentRunFolder, 'Run');
          
          if( strf > 0 )
          
            RunCount = RunCount + 1;
            
          endif
        
        endfor