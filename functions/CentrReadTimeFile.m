function [GALnrDuration] = CentrReadTimeFile(RunPath, TimeFileCount)
        
%        RunPath = 'home/meshlab1/Resilio Sync Tim Brockmann/CHaChA/centralized/software/logs/TestData_250K/MCS3/20180516_17-26-17_MAN1_MCS3_MaxProcs0_50Runs/Node_1/Run_1'
%        RunPath = './20180227_17-15-21_MAN1/Node_1/Run_9';
%        TimeFileCount = 24;
        
        GALcounter = 0;
        
        CentrTimeFiles = dir(RunPath);
        TimeFileList = {CentrTimeFiles.name};
        charTimeFileList = char(TimeFileList);
        
        GALnrDuration = zeros(TimeFileCount,2);
        
        for EnterTimeFiles=1:(numel(TimeFileList))
          
%          printf("\ngo TimeFile");
          %printf("any file\n");
          strf = 0;
          currentTimeFile = CentrTimeFiles(EnterTimeFiles).name;
          strf = strfind(currentTimeFile, 'time_netcat');
          
          if( strf > 0 )
            GALcounter = GALcounter + 1;
%            printf("\ngo");
%            printf("%s \n", char(currentTimeFile));
            
            currentTimeFileName = strsplit(currentTimeFile, '.');
            LastIPField = currentTimeFileName(1,numel(currentTimeFileName)-1);
            
            charLastIPField = char(LastIPField);
            %printf("Node IP: %s \n", charLastIPField);
            
            GALnrDuration(GALcounter,1) = str2num(charLastIPField)-9;
            
            data = textread([RunPath,'/',currentTimeFile],'%s','delimiter', '\n','whitespace','');
            
            for i=1:numel(data)
                
              Line = char(data(i,1));
              LineSplit = strsplit(Line);
              firstEntry = char(LineSplit(1,1));
              
              switch(firstEntry)
                case "real"
                  RealDurationLine = strsplit(Line, {"real","  ","m","s"});
                  min2millisec = str2num(char(RealDurationLine(1,2)))*60*1000;
                  sec2millisec = str2num(char(RealDurationLine(1,3)))*1000;
                  overallmillisec = min2millisec + sec2millisec;
                  GALnrDuration(GALcounter,2) = overallmillisec;
              endswitch

            endfor    
             
          endif
        
      endfor
      
      %GALnrDuration