function [] = PlotCM(ClusterMember, isMaster, CMString,CHead, ColorNumber, ClusterChannel)
  
%  FontName  = 'Times New Roman';
  FontName  = 'Nimbus Rom No 9L';
%  FontName  = 'Arial';
  
  Color = ChooseColor(str2num(ClusterChannel));
  A = ClusterMember;
  if(isMaster == 0)
  if(CHead == 1)
  
    %plot(A(:,1),A(:,2),'o','MarkerSize',15,'MarkerEdgeColor',Color,'markerfacecolor',Color);
    plot(A(:,1),A(:,2),'o','MarkerSize',18,'MarkerEdgeColor',Color);
%    text (A(:,1), A(:,2)+0.3, ["C.",ClusterChannel], 'color', Color,'horizontalalignment', "center",'interpreter',"tex" );
    text (A(:,1), A(:,2)+0.3, ["C.",ClusterChannel], 'color', Color,'horizontalalignment', "center");

    %text (A(:,1), A(:,2), CMString,'horizontalalignment', "center" );
    
  endif
  
  if(CHead == 0)
  
    plot(A(:,1),A(:,2),'o','MarkerSize',15,'MarkerEdgeColor',Color,'markerfacecolor',"none");
%    text (A(:,1), A(:,2)+0.03, CMString,'horizontalalignment', "center",'interpreter',"tex");
    text (A(:,1), A(:,2)+0.03, CMString,'horizontalalignment', "center");

%    set(gca, 'FontName', 'Nimbus Rom No 9L', 'FontSize', 25)
    
  endif
  endif
  
  if(isMaster == 1)
     plot(A(:,1),A(:,2),'s','MarkerSize',18, 'MarkerEdgeColor','black'); 
  endif

%X = text(3,0,'{\fontname{Nimbus Rom No 9L} Das ist ein Test für Schriftarten x_{y}^{34}}', 'color', Color,'horizontalalignment', "center",'Interpreter','tex' );
%set(X, 'FontName', FontName, 'FontSize', 25)

%xlabel ('{\bf H} = a {\bf V}')