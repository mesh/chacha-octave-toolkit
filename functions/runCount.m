function [RunCount] = runCount(path)
  
  %path = '../../../distributed/software/logs/0_clustering/';
  
  Adir = dir(path); %save folder structure into a struct
  a = size(Adir);
  FolderList = {Adir.name}; %StringField to Char_array
  RunCount = 0;
  
for clusteringRuns=1:(numel(FolderList))
  currentFolder = Adir(clusteringRuns).name;
  isFolder = ~isletter(currentFolder);
  trueFolder = sum(isFolder(:) == 1);
  if( trueFolder >= 10 )
    RunCount = RunCount + 1;
  endif
endfor

%Rate = zeros(RunCount,2);
%TF = 0;
%for clusteringRuns=1:(numel(FolderList))
%  currentFolder = Adir(clusteringRuns).name;
%  isFolder = ~isletter(currentFolder);
%  trueFolder = sum(isFolder(:) == 1);
%
%  if (trueFolder >= 10)
%    TF = TF + 1;
%    %printf("Entering Folder: %s \n", currentFolder);
%    Rate(TF,1) = TF;
%    printf("-> Entered a Cluster Folder \n");
%    cf_for_filename = currentFolder;
%    newpath = [path, currentFolder];
%    Bdir = dir(newpath);
%    FolderList1 = {Bdir.name};
%    
%    for yFolder=1:(numel(FolderList1))
%    strf = 0;
%    currentFolder1 = Bdir(yFolder).name;
%    strf = strfind(currentFolder1, 'Node');
%    if(strf > 0)
%      newpath1 = [newpath,'/', currentFolder1];
%      Cdir = dir(newpath1);
%      FolderList2 = {Cdir.name};
%      printf("Entered a NodeFolder \n");
%    endif
%    endfor
%  endif
%endfor
