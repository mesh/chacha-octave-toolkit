%=================================================================================================================================
%Switches
%=================================================================================================================================

Show_Terminal_Outputs = 1;
Open_Created_Figures_In_Octave = 0;		# TODO: seems to be required on Linux machines with proprietary nvidia driver
Create_Graphic_Output_Files = 1;
Create_TXT_Summary = 1;

# TODO: for monitoring eval. diagrams -> adjust to MAN1 case (MCS0: 60s | MCS3: 20s)
cent_Max_Y_Axis_Border = 16;
dist_Max_Y_Axis_Border = 18;
compare_Max_Y_Axis_Border = 18;

%=================================================================================================================================
%Paths
%=================================================================================================================================

%---------------------------------------------------------------------------------------------------------------------------------
%Path for Script: '0_clustering_eval.m'

%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/20180307_Clustering_50Runs_WNPR_old_productCentralityNPR';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/20180525_Clustering_50Runs_WNPR_new_productCentralityShareNormalizedNPR';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/tmp';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/single_case_6_cluster';

%DIFFERENT TOPOLOGIES
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/Ring';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/Plus';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/Triangle';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/L';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/Line';
Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/AllNodes';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/2x2';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/3x3';
%Clustering_Evaluation_Path = '../../../distributed/software/logs/1_clustering/4x4';

%---------------------------------------------------------------------------------------------------------------------------------

%---------------------------------------------------------------------------------------------------------------------------------
%Path for Script: '1_monitoring_distr.m'

%Monitoring_Distributed_Path = '../../../distributed/software/logs/2_monitoring/MCS0';
Monitoring_Distributed_Path = '../../../distributed/software/logs/2_monitoring/MCS3';
%---------------------------------------------------------------------------------------------------------------------------------

%---------------------------------------------------------------------------------------------------------------------------------
%Path for Script: '2_monitoring_centr.m'

%Monitoring_Centralized_Path = '../../../centralized/software/logs/TestData_250K/MCS0';
Monitoring_Centralized_Path = '../../../centralized/software/logs/TestData_250K/MCS3';
%---------------------------------------------------------------------------------------------------------------------------------

%---------------------------------------------------------------------------------------------------------------------------------
%Path's for Script: '3_compare_monitoring'
%For comparison of monitoring results in the four different network-setups, 
%the scripts 0..2 have to have run before. It is also necessary to set the 
%'Create_TXT_Summary'-switch to the value of 1, to create the required TXT-Files
%inside the folders as stated below:

%Comp_Mon_Centr_MAN1_Path = '../../../centralized/software/logs/TestData_250K/MCS0/20180308_14-01-18_MAN1_MCS0_MaxProcs0_50Runs';											# higher TX power G15/22 (12dBm)
%
%Comp_Mon_Centr_MAN1_Path = '../../../centralized/software/logs/TestData_250K/MCS3/20180517_12-54-10_MAN1_MCS3_MaxProcs0_50Runs';											# higher TX power G15/22 (17dBm)
%Comp_Mon_Centr_MAN1_Path = '../../../centralized/software/logs/TestData_250K/MCS3/20180518_15-37-47_MAN1_MCS3_MaxProcs0_RandomOrder_50Runs';								# higher TX power G15/22 (17dBm)
Comp_Mon_Centr_MAN1_Path = '../../../centralized/software/logs/TestData_250K/MCS3/20180523_20-11-54_MAN1_MCS3_MaxProcs0_RandomOrder_minNF_50Runs';							# higher TX power G15/22/G2/G8 (17dBm), minNF

%Comp_Mon_Centr_MAN15_Path = '../../../centralized/software/logs/TestData_250K/MCS0/20180308_15-48-09_MAN15_MCS0_MaxProcs0_50Runs';											# higher TX power G15/22 (12dBm)
%
%Comp_Mon_Centr_MAN15_Path = '../../../centralized/software/logs/TestData_250K/MCS3/20180517_13-21-33_MAN15_MCS3_MaxProcs0_50Runs';											# higher TX power G15/22 (17dBm)
%Comp_Mon_Centr_MAN15_Path = '../../../centralized/software/logs/TestData_250K/MCS3/20180518_16-18-27_MAN15_MCS3_MaxProcs0_RandomOrder_50Runs';								# higher TX power G15/22 (17dBm)
Comp_Mon_Centr_MAN15_Path = '../../../centralized/software/logs/TestData_250K/MCS3/20180523_18-42-47_MAN15_MCS3_MaxProcs0_RandomOrder_minNF_50Runs';						# higher TX power G15/22/G2/G8 (17dBm), minNF

%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS0/20180509_18-33-57_DIST_SingleChan_Const4_250K_MCS0_IF1only_50Runs';					# equal TX powers ( 8dBm)
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS0/20180516_18-48-48_DIST_SingleChan_Const4_250K_MCS0_IF1only_50Runs';					# equal TX powers ( 8dBm)
%
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180517_15-44-18_DIST_SingleChan_Const4_250K_MCS3_IF1only_50Runs';					# equal TX powers (14dBm)
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180517_17-36-56_DIST_SingleChan_Const4_250K_MCS3_IF1only_50Runs';					# higher TX power G15/22 (17dBm)
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180519_14-01-20_DIST_SingleChan_Const4_250K_MCS3_IF1only_RandomOrder_50Runs';		# higher TX power G15/22 (17dBm)
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180520_19-10-14_DIST_SingleChan_Const4_250K_MCS3_IF1only_RandomOrder_minNF_50Runs';	# higher TX power G15/22 (17dBm), minNF
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180521_18-26-38_DIST_SingleChan_Const4_250K_MCS3_IF1only_RandomOrder_minNF_50Runs';	# higher TX power G15/22 (17dBm), minNF
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180523_11-06-19_DIST_SingleChan_Const4_250K_MCS3_IF1only_RandomOrder_minNF_50Runs';	# higher TX power G15/22/G2/G8 (17dBm), minNF
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180528_17-18-16_DIST_SingleChan_Const5_250K_MCS3_IF1only_RandomOrder_minNF_rootMode0_50Runs';
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180529_12-01-34_DIST_SingleChan_Const5_250K_MCS3_IF1only_RandomOrder_minNF_rootMode2_50Runs';
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180529_13-52-03_DIST_SingleChan_Const5_250K_MCS3_IF1only_RandomOrder_minNF_rootMode0_50Runs';
%Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180529_17-34-03_DIST_SingleChan_Const5_250K_MCS3_IF1only_RandomOrder_minNF_rootMode0_50Runs';
Comp_Mon_Distr_SingChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180530_14-50-31_DIST_SingleChan_Const5_250K_MCS3_IF1only_RandomOrder_minNF_rootMode2_clusterMeshIDs_50Runs'; %TODO

%Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS0/20180510_15-44-00_DIST_MultiChan_Const4_250K_MCS0_IF1only_50Runs';					# equal TX powers ( 8dBm)
%Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS0/20180517_10-32-08_DIST_MultiChan_Const4_250K_MCS0_IF1only_50Runs';					# equal TX powers ( 8dBm)
%
%Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180517_16-26-18_DIST_MultiChan_Const4_250K_MCS3_IF1only_50Runs';					# equal TX powers (14dBm)
%Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180517_18-57-07_DIST_MultiChan_Const4_250K_MCS3_IF1only_50Runs';					# higher TX power G15/22 (17dBm)
%Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180519_15-55-17_DIST_MultiChan_Const4_250K_MCS3_IF1only_RandomOrder_50Runs';		# higher TX power G15/22 (17dBm)
%Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180520_20-49-20_DIST_MultiChan_Const4_250K_MCS3_IF1only_RandomOrder_minNF_50Runs';	# higher TX power G15/22 (17dBm), minNF
%Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180521_21-31-22_DIST_MultiChan_Const4_250K_MCS3_IF1only_RandomOrder_minNF_50Runs';	# higher TX power G15/22 (17dBm), minNF
%Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180523_14-25-47_DIST_MultiChan_Const4_250K_MCS3_IF1only_RandomOrder_minNF_50Runs';	# higher TX power G15/22/G2/G8 (17dBm), minNF
%Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180528_18-11-10_DIST_MultiChan_Const5_250K_MCS3_IF1only_RandomOrder_minNF_rootMode0_50Runs';
Comp_Mon_Distr_MultChan_Path = '../../../distributed/software/logs/2_monitoring/MCS3/20180530_15-39-14_DIST_MultiChan_Const5_250K_MCS3_IF1only_RandomOrder_minNF_rootMode2_clusterMeshIDs_50Runs'; %TODO

Comp_Mon_Output_Folder = '../../../monitoring_comparison';
%---------------------------------------------------------------------------------------------------------------------------------

